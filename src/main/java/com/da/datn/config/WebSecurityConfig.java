package com.da.datn.config;

import com.da.datn.config.security.*;
import com.da.datn.enums.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.logout.LogoutFilter;
import org.springframework.web.servlet.HandlerExceptionResolver;

@Configuration
@EnableWebSecurity()
@EnableMethodSecurity()
public class WebSecurityConfig {

    private TokenProvider tokenProvider;
    @Autowired
    @Qualifier("handlerExceptionResolver")
    private HandlerExceptionResolver resolver;

    public static String[] mergeArrays(String[]... arrays) {
        int totalLength = 0;
        for (String[] array : arrays) {
            totalLength += array.length;
        }
        String[] result = new String[totalLength];
        int destPos = 0;
        for (String[] array : arrays) {
            System.arraycopy(array, 0, result, destPos, array.length);
            destPos += array.length;
        }
        return result;
    }

    @Autowired
    public void setTokenProvider(@Lazy TokenProvider tokenProvider) {
        this.tokenProvider = tokenProvider;
    }

    public ExceptionHandlerFilter exceptionHandlerFilter() {
        return new ExceptionHandlerFilter(resolver);
    }

    @Bean
    @Qualifier("BCrypt")
    public PasswordEncoder passwordEncoder() {
        return new PasswordEncoder() {
            @Override
            public String encode(CharSequence rawPassword) {
                return BCrypt.hashpw(rawPassword.toString(), BCrypt.gensalt(4));
            }

            @Override
            public boolean matches(CharSequence rawPassword, String encodedPassword) {
                return BCrypt.checkpw(rawPassword.toString(), encodedPassword);
            }
        };
    }

    public TokenAuthenticationFilter tokenAuthenticationFilter() {
        return new TokenAuthenticationFilter(tokenProvider);
    }

    public CustomAuthenticationFilter customAuthenticationFilter() {
        CustomAuthenticationFilter customAuthenticationFilter = new CustomAuthenticationFilter(tokenProvider);
        customAuthenticationFilter.setAuthenticationFailureHandler(authenticationFailureHandler());
        customAuthenticationFilter.setFilterProcessesUrl("/api/v1/login");
        return customAuthenticationFilter;
    }

      @Bean
      protected SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
          http
                  .csrf().disable()
                  .sessionManagement()
                  .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                  .and().formLogin().disable()
                  .httpBasic().disable()
                  .exceptionHandling()
                  .authenticationEntryPoint(new RestAuthenticationEntryPoint())
                  .and()
                  .authorizeHttpRequests((authz) -> authz
                          .requestMatchers(permitAllPaths()).permitAll()
                          .requestMatchers("/api/v1/admin/**", "/api/v1/contract/accept-or-reject").hasAuthority("ADMIN")
                          .requestMatchers(permitCommonPaths()).hasAnyAuthority("ADMIN", "MANAGEMENT_FINANCIAL", "ADMINISTRATIVE")
                          .requestMatchers(permitRoleManagerFinancialPaths()).hasAnyAuthority("ADMIN", "MANAGEMENT_FINANCIAL")
                          .requestMatchers(permitRoleAdminstrativePaths()).hasAnyAuthority("ADMIN", "ADMINISTRATIVE")

                          .requestMatchers(HttpMethod.GET, "/api/v1/request").hasAnyAuthority("ADMIN", "STUDENT", "MANAGEMENT_FINANCIAL", "ADMINISTRATIVE")
                          .requestMatchers(HttpMethod.POST, "/api/v1/request/**").hasAnyAuthority("ADMIN", "STUDENT", "MANAGEMENT_FINANCIAL", "ADMINISTRATIVE")
                          .requestMatchers(HttpMethod.PUT, "/api/v1/request/**").hasAnyAuthority("ADMIN", "STUDENT", "MANAGEMENT_FINANCIAL", "ADMINISTRATIVE")
                          .requestMatchers(HttpMethod.OPTIONS, "/api/v1/request/**").hasAnyAuthority("ADMIN", "STUDENT", "MANAGEMENT_FINANCIAL", "ADMINISTRATIVE")
                          .requestMatchers(HttpMethod.DELETE, "/api/v1/request/**").hasAnyAuthority("ADMIN", "STUDENT", "MANAGEMENT_FINANCIAL", "ADMINISTRATIVE")
                          .requestMatchers("/api/v1/request/accept-or-reject").hasAnyAuthority("ADMIN", "MANAGEMENT_FINANCIAL", "ADMINISTRATIVE")
                          .requestMatchers("/api/v1/request/yc").hasAnyAuthority("ADMIN", "MANAGEMENT_FINANCIAL", "ADMINISTRATIVE")

                          .requestMatchers(HttpMethod.GET, "/api/v1/room/**").hasAnyAuthority("ADMIN","MANAGEMENT_FINANCIAL", "ADMINISTRATIVE")
                          .requestMatchers(HttpMethod.POST, "/api/v1/room/**").hasAnyAuthority("ADMIN","ADMINISTRATIVE")
                          .requestMatchers(HttpMethod.PUT, "/api/v1/room/**").hasAnyAuthority("ADMIN","ADMINISTRATIVE")
                          .requestMatchers(HttpMethod.OPTIONS, "/api/v1/room/**").hasAnyAuthority("ADMIN","ADMINISTRATIVE")
                          .requestMatchers(HttpMethod.DELETE, "/api/v1/room/**").hasAnyAuthority("ADMIN","ADMINISTRATIVE")

                          .requestMatchers(HttpMethod.GET, "/api/v1/student/**").hasAnyAuthority("ADMIN","MANAGEMENT_FINANCIAL", "ADMINISTRATIVE")
                          .requestMatchers(HttpMethod.POST, "/api/v1/student/**").hasAnyAuthority("ADMIN","ADMINISTRATIVE")
                          .requestMatchers(HttpMethod.PUT, "/api/v1/student/**").hasAnyAuthority("ADMIN","ADMINISTRATIVE")
                          .requestMatchers(HttpMethod.OPTIONS, "/api/v1/student/**").hasAnyAuthority("ADMIN","ADMINISTRATIVE")
                          .requestMatchers(HttpMethod.DELETE, "/api/v1/student/**").hasAnyAuthority("ADMIN","ADMINISTRATIVE")

//                          .requestMatchers(HttpMethod.GET, "/api/v1/contract/**").hasAnyAuthority(   "ADMIN","ADMINISTRATIVE")
//                          .requestMatchers(HttpMethod.POST, "/api/v1/contract/**").hasAnyAuthority("ADMIN","ADMINISTRATIVE")
//                          .requestMatchers(HttpMethod.PUT, "/api/v1/contract/**").hasAnyAuthority("ADMIN","ADMINISTRATIVE")
//                          .requestMatchers(HttpMethod.OPTIONS, "/api/v1/contract/**").hasAnyAuthority("ADMIN","ADMINISTRATIVE")
//                          .requestMatchers(HttpMethod.DELETE, "/api/v1/contract/**").hasAnyAuthority("ADMIN","ADMINISTRATIVE")


                          .anyRequest().authenticated()
                  );
          http.addFilterBefore(customAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class);
          http.addFilterBefore(tokenAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class);
          http.addFilterBefore(exceptionHandlerFilter(), LogoutFilter.class);
          return http.build();
      }

    public String[] permitCommonPaths() {
        String[] paths = new String[]{
//                "/api/v1/request/**",
        };
        return paths;
    }

    public String[] permitRoleManagerFinancialPaths() {
        String[] paths = new String[]{
                "/api/v1/statitics/**"
        };
        return paths;
    }

    public String[] permitRoleAdminstrativePaths() {
        String[] paths = new String[]{
                "/api/v1/contract/**"
        };
        return paths;
    }

    public String[] permitAllPaths() {
        String[] additionalPaths = new String[]{
                "/api/v1/test",
                "/api/v1/user/reset-password",
                "/api/v1/forgot-password",
                "/api/v1/refreshToken",
                "/api/v1/public/**",
                "/api/v1/admin/register",
                "api/v1/user/**",
                "/api/v1/check",

        };
        return additionalPaths;
    }

    @Bean
    public AuthenticationFailureHandler authenticationFailureHandler() {
        return new CustomAuthenticationFailureHandler();
    }

}
