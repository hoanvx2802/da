package com.da.datn.config.security;

import com.da.datn.config.exception.ApplicationRuntimeException;
import com.da.datn.dto.res.ResponseDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
@Slf4j
public class ControllerAdvice {

    @ExceptionHandler(ApplicationRuntimeException.class)
    public ResponseEntity<?> handleAllRemainingException(ApplicationRuntimeException ex) {
        log.warn("ApplicationRuntimeException: {}", ex.getMessage());
        return new ResponseEntity<>(ResponseDTO.builder().message(ex.getMessage()).data(ex.getData()).build(), ex.getHttpStatus());
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<?> handleAllRemainingException(Exception ex) {
        log.warn("Exception: {}", ex.getMessage(), ex);
        return new ResponseEntity<>(ResponseDTO.builder().message(ex.getMessage()).build(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
