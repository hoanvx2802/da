package com.da.datn.config.security;

import com.da.datn.entity.Users;
import com.da.datn.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.Objects;

@Service
@RequiredArgsConstructor
public class CustomUserDetailsService implements UserDetailsService {
    private final UserService userService;

    @Override
    public UserDetails loadUserByUsername(String username)
            throws UsernameNotFoundException {
        Users user = userService.findByUserName(username);
        if (ObjectUtils.isEmpty(user)) {
            throw new UsernameNotFoundException("Không tìm thấy người dùng : " + username);
        }
        if (Objects.isNull(user.getActive()) || !user.getActive()) {
            throw new UsernameNotFoundException("Người dùng bị khóa : " + username);
        }
        return UserPrincipal.create(user);
    }
}
