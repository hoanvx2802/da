package com.da.datn.controller;

import com.da.datn.config.exception.ApplicationRuntimeException;
import com.da.datn.dto.CiCoutDTO;
import com.da.datn.dto.PageDTO;
import com.da.datn.entity.CheckInCheckout;
import com.da.datn.entity.Student;
import com.da.datn.repository.CiCoutRepository;
import com.da.datn.repository.StudentRepository;
import com.da.datn.service.CiCoutService;
import com.da.datn.utils.PageUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/check")
public class CheckInCheckOutController {
    final CiCoutRepository ciCoutRepository;
    final StudentRepository studentRepository;
    final CiCoutService ciCoutService;

    @PostMapping
    public String checkInCheckOut(@RequestBody CiCoutDTO ciCoutDTO) {
        Student student = studentRepository.findByUsername(ciCoutDTO.getStudentName()).orElseThrow(() -> new ApplicationRuntimeException("Không tìm thấy sinh viên"));
        try {
            ciCoutRepository.save(
                    CheckInCheckout.builder()
                            .studentName(student.getUsername())
                            .studentId(student.getId())
                            .image(ciCoutDTO.getImage())
                            .timeCheck(ciCoutDTO.getTimeCheck())
                            .status(ciCoutDTO.getStatus())
                            .build());
            return "success";
        } catch (Exception e) {
            throw new ApplicationRuntimeException("fail");
        }
    }

    @GetMapping
    public ResponseEntity<PageDTO<CheckInCheckout>> checkInCheckOut(@RequestParam(required = false, defaultValue = "1") Integer currentPage,
                                                                    @RequestParam(required = false, defaultValue = "10") Integer limit,
                                                                    @RequestParam(defaultValue = "timeCheck-DESC") String sort,
                                                                    @RequestParam(defaultValue = "%") String keyword,
                                                                    @RequestParam(required = false) Set<String> status) {
        Pageable pageable = PageUtils.getPageable(currentPage, limit, sort);
        return new ResponseEntity<>(ciCoutService.getAll(pageable, keyword, status), HttpStatus.OK);
    }

}
