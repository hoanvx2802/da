package com.da.datn.controller;

import com.da.datn.dto.ContractDTO;
import com.da.datn.dto.PageDTO;
import com.da.datn.dto.req.AcceptOrRejectContractDTO;
import com.da.datn.dto.res.ContractResponse;
import com.da.datn.entity.Contract;
import com.da.datn.service.ContractService;
import com.da.datn.utils.PageUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/contract")
public class ContractController {

    final ContractService contractService;

    @GetMapping
    public ResponseEntity<PageDTO<ContractResponse>> findAllContract(@RequestParam(required = false, defaultValue = "1") Integer currentPage,
                                                                     @RequestParam(required = false, defaultValue = "10") Integer limit,
                                                                     @RequestParam(defaultValue = "createAt-DESC") String sort,
                                                                     @RequestParam(defaultValue = "%") String keyword,
                                                                     @RequestParam(required = false) Set<String> status) {
        Pageable pageable = PageUtils.getPageable(currentPage, limit, sort);
        return new ResponseEntity<>(contractService.findAllContract(pageable, keyword, status), HttpStatus.OK);
    }

    @GetMapping("/yc")
    public ResponseEntity<PageDTO<ContractResponse>> findAllManagerContract(@RequestParam(required = false, defaultValue = "1") Integer currentPage,
                                                                            @RequestParam(required = false, defaultValue = "10") Integer limit,
                                                                            @RequestParam(defaultValue = "createAt-DESC") String sort,
                                                                            @RequestParam(defaultValue = "%") String keyword,
                                                                            @RequestParam(required = false) Set<String> status,
                                                                            @RequestParam(defaultValue = "false") Boolean isAccept) {
        Pageable pageable = PageUtils.getPageable(currentPage, limit, sort);
        return new ResponseEntity<>(contractService.findAllManagerContract(pageable, keyword, status, isAccept), HttpStatus.OK);
    }

    @PostMapping("/accept-or-reject")
    public ResponseEntity<Contract> acceptContract(@RequestBody AcceptOrRejectContractDTO dto) {
        return new ResponseEntity<>(contractService.accpectOrRejectContract(dto), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Contract> createContract(@RequestBody ContractDTO contractDTO) {
        return new ResponseEntity<>(contractService.create(contractDTO), HttpStatus.OK);
    }

    @PostMapping("/extend")
    public ResponseEntity<Contract> extendContract(@RequestBody ContractDTO contractDTO) {
        return new ResponseEntity<>(contractService.extendContract(contractDTO), HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<Contract> updateContract(@RequestBody ContractDTO contractDTO) {
        return new ResponseEntity<>(contractService.update(contractDTO), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteContract(@PathVariable Long id) {
        contractService.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }


}
