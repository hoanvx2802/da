package com.da.datn.controller;

import com.da.datn.dto.EmployeeDTO;
import com.da.datn.dto.PageDTO;
import com.da.datn.service.AdminService;
import com.da.datn.service.EmployeeService;
import com.da.datn.utils.PageUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/employee")
public class EmployeeController {

    final AdminService adminService;

    final EmployeeService employeeService;

    @PostMapping
    public ResponseEntity<?> createStaff(@RequestBody EmployeeDTO employeeDTO) {
        return new ResponseEntity<>(employeeService.createEmployee(employeeDTO), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> removeStaff(@PathVariable Long id) {
        employeeService.removeEmployee(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<PageDTO<EmployeeDTO>> getAllStaff(@RequestParam(required = false, defaultValue = "1") Integer currentPage,
                                                            @RequestParam(required = false, defaultValue = "10") Integer limit,
                                                            @RequestParam(defaultValue = "fullName-ASC") String sort,
                                                            @RequestParam(defaultValue = "%") String keyword) {
        Pageable pageable = PageUtils.getPageable(currentPage, limit, sort);
        return new ResponseEntity<>(employeeService.getAllEmployee(pageable, keyword), HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<?> updateEmployee(@RequestBody EmployeeDTO employeeDTO) {
        return new ResponseEntity<>(employeeService.updateEmployee(employeeDTO), HttpStatus.OK);
    }


}
