package com.da.datn.controller;

import com.da.datn.service.impl.MinIOService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/public/image")
public class MinIOController {
    final MinIOService minIOService;

    @PostMapping(value = "/avt")
    public String uploadFile(@RequestParam("file") MultipartFile file) {
        return minIOService.objectUpload(file, "avatar");
    }

    @GetMapping("/avt")
    public String getAvt(@RequestParam("filename") String filename, @RequestParam("key") String key) {
        return minIOService.getObject(filename, key);
    }

    @PostMapping(value = "/checkinout")
    public String uploadFileCheckInOut(@RequestParam("file") MultipartFile file) {
        return minIOService.objectUpload(file, "checkinout");
    }
}
