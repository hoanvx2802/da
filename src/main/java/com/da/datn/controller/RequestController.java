package com.da.datn.controller;

import com.da.datn.dto.PageDTO;
import com.da.datn.dto.RequestDTO;
import com.da.datn.dto.req.AcceptOrRejectRequestDTO;
import com.da.datn.entity.Request;
import com.da.datn.service.RequestService;
import com.da.datn.utils.PageUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.Set;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/request")
public class RequestController {

    final RequestService requestService;

    @GetMapping
    public ResponseEntity<PageDTO<RequestDTO>> getManagerRequest(@RequestParam(required = false, defaultValue = "1") Integer currentPage,
                                                                 @RequestParam(required = false, defaultValue = "10") Integer limit,
                                                                 @RequestParam(defaultValue = "createAt-DESC") String sort,
                                                                 @RequestParam(defaultValue = "%") String keyword,
                                                                 @RequestParam(required = false) Set<String> status,
                                                                 @RequestParam(required = false) @DateTimeFormat(pattern = "dd/MM/yyyy") LocalDate from,
                                                                 @RequestParam(required = false) @DateTimeFormat(pattern = "dd/MM/yyyy") LocalDate to) {
        Pageable pageable = PageUtils.getPageable(currentPage, limit, sort);
        return new ResponseEntity<>(requestService.findAllRequest(pageable, keyword, status, from, to), HttpStatus.OK);
    }

    @GetMapping("/yc")
    public ResponseEntity<PageDTO<RequestDTO>> getRequest(@RequestParam(required = false, defaultValue = "1") Integer currentPage,
                                                          @RequestParam(required = false, defaultValue = "10") Integer limit,
                                                          @RequestParam(defaultValue = "createAt-DESC") String sort,
                                                          @RequestParam(defaultValue = "%") String keyword,
                                                          @RequestParam(defaultValue = "false") Boolean isAccept,
                                                          @RequestParam(required = false) Set<String> status,
                                                          @RequestParam(required = false) @DateTimeFormat(pattern = "dd/MM/yyyy") LocalDate from,
                                                          @RequestParam(required = false) @DateTimeFormat(pattern = "dd/MM/yyyy") LocalDate to) {
        Pageable pageable = PageUtils.getPageable(currentPage, limit, sort);
        return new ResponseEntity<>(requestService.findAllManagerRequest(pageable, keyword, status, isAccept, from, to), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Request> create(@RequestBody RequestDTO requestDTO) {
        return new ResponseEntity<>(requestService.create(requestDTO), HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<Request> update(@RequestBody RequestDTO requestDTO) {
        return new ResponseEntity<>(requestService.update(requestDTO), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable Long id) {
        requestService.delete(id);
        return ResponseEntity.ok().build();
    }

    @PostMapping("/accept-or-reject")
    public ResponseEntity<RequestDTO> acceptOrReject(@RequestBody AcceptOrRejectRequestDTO acceptOrRejectRequestDTO) {
        return new ResponseEntity<>(requestService.acceptOrRejectRequest(acceptOrRejectRequestDTO), HttpStatus.OK);
    }
}
