package com.da.datn.controller;

import com.da.datn.config.exception.ApplicationRuntimeException;
import com.da.datn.dto.PageDTO;
import com.da.datn.dto.RoomDTO;
import com.da.datn.entity.Contract;
import com.da.datn.entity.Room;
import com.da.datn.repository.ContractRepository;
import com.da.datn.service.RoomService;
import com.da.datn.utils.PageUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/room")
public class RoomController {

    final RoomService roomService;

    @GetMapping
    public ResponseEntity<PageDTO<RoomDTO>> getRoom(@RequestParam(required = false, defaultValue = "1") Integer currentPage,
                                                    @RequestParam(required = false, defaultValue = "10") Integer limit,
                                                    @RequestParam(defaultValue = "id-ASC") String sort,
                                                    @RequestParam(required = false, name = "status") Set<String> status,
                                                    @RequestParam(defaultValue = "%") String building,
                                                    @RequestParam(required = false) Boolean isContract,
                                                    @RequestParam(defaultValue = "%") String roomNumber) {
        Pageable pageable = PageUtils.getPageable(currentPage, limit, sort);
        return new ResponseEntity<>(roomService.getAllRooms(pageable, status, building, roomNumber, isContract), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Room> createRoom(@RequestBody RoomDTO roomDTO) {
        return new ResponseEntity<>(roomService.createRoom(roomDTO), HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<Room> updateRoom(@RequestBody RoomDTO roomDTO) {
        return new ResponseEntity<>(roomService.updateRoom(roomDTO), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteRoom(@PathVariable Long id) {
        roomService.deleteRoom(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}