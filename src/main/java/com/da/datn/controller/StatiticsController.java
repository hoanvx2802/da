package com.da.datn.controller;

import com.da.datn.dto.StatiticsDTO;
import com.da.datn.service.StatiticsService;
import jakarta.validation.constraints.Positive;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/statitics")
public class StatiticsController {

    final StatiticsService statiticsService;

    @GetMapping
    public ResponseEntity<?> statitics() {
        return new ResponseEntity<>(statiticsService.calcBillRoom(), HttpStatus.OK);
    }

    @PostMapping("/{month}")
    public ResponseEntity<StatiticsDTO> statiticsForMonth(@PathVariable String month) {
        return new ResponseEntity<>(statiticsService.getStatiticsForMonth(month), HttpStatus.OK);
    }

    @PostMapping("/room")
    public ResponseEntity<String> statiticsForRoom(@Positive Long idRoom, @Positive Long waterPrice, @Positive Long electricPrice) {
        return new ResponseEntity<>(statiticsService.calcBillOneRoom(idRoom, waterPrice, electricPrice), HttpStatus.OK);
    }
}
