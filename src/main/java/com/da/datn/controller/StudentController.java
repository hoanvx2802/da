package com.da.datn.controller;

import com.da.datn.dto.PageDTO;
import com.da.datn.dto.StudentDTO;
import com.da.datn.service.AdminService;
import com.da.datn.service.StudentService;
import com.da.datn.utils.PageUtils;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/student")
public class StudentController {

    final StudentService studentService;

    final AdminService adminService;

    @GetMapping
    public ResponseEntity<PageDTO<StudentDTO>> getAllStudents(@RequestParam(required = false, defaultValue = "1") Integer currentPage,
                                                              @RequestParam(required = false, defaultValue = "10") Integer limit,
                                                              @RequestParam(defaultValue = "fullName-ASC") String sort,
                                                              @RequestParam(required = false) Boolean isContract,
                                                              @RequestParam(defaultValue = "%") String keyword) {
        Pageable pageable = PageUtils.getPageable(currentPage, limit, sort);
        return new ResponseEntity<>(studentService.getAllStudents(pageable, keyword, isContract), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<?> createStudent(@RequestBody StudentDTO studentDTO) {
        return new ResponseEntity<>(studentService.createStudent(studentDTO), HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<?> updateStudent(@RequestBody StudentDTO studentDTO) {
        return new ResponseEntity<>(studentService.updateStudent(studentDTO), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    @Transactional
    public ResponseEntity<?> removeStudent(@PathVariable Long id) {
        studentService.removeStudent(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
