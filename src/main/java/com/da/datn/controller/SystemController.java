package com.da.datn.controller;

import com.da.datn.dto.PageDTO;
import com.da.datn.dto.StudentDTO;
import com.da.datn.dto.SystemConfigDTO;
import com.da.datn.entity.SystemConfig;
import com.da.datn.service.SystemService;
import com.da.datn.utils.PageUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1")
public class SystemController {

    final SystemService systemConfigService;

    @PostMapping("/system")
    public ResponseEntity<SystemConfig> addSystemConfig(@RequestBody SystemConfigDTO systemConfigDTO) {
        return new ResponseEntity<>(systemConfigService.addSystemConfig(systemConfigDTO.getKeyword(), systemConfigDTO.getValue()), HttpStatus.OK);
    }

    @PutMapping("/system")
    public ResponseEntity<SystemConfig> updateSystemConfig(@RequestBody SystemConfigDTO systemConfigDTO) {
        return new ResponseEntity<>(systemConfigService.updateSystemConfig(systemConfigDTO.getKeyword(), systemConfigDTO.getValue()), HttpStatus.OK);
    }

    @GetMapping("/system")
    public ResponseEntity<PageDTO<SystemConfig>> getAllStudents(@RequestParam(required = false, defaultValue = "1") Integer currentPage,
                                                              @RequestParam(required = false, defaultValue = "10") Integer limit,
                                                              @RequestParam(defaultValue = "keyword-ASC") String sort,
                                                              @RequestParam(defaultValue = "%") String keyword) {
        Pageable pageable = PageUtils.getPageable(currentPage, limit, sort);
        return new ResponseEntity<>(systemConfigService.findAllSystem(pageable, keyword), HttpStatus.OK);
    }

    @PostMapping("/public/system")
    public void sendBill() {
        systemConfigService.cronSendBill();
    }
}
