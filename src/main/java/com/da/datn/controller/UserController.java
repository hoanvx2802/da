package com.da.datn.controller;

import com.da.datn.dto.common.UserDTO;
import com.da.datn.dto.req.ChangePasswordDTO;
import com.da.datn.dto.req.RefreshTokenRequest;
import com.da.datn.dto.res.ResponseDTO;
import com.da.datn.entity.Users;
import com.da.datn.repository.UserRepository;
import com.da.datn.service.AdminService;
import com.da.datn.service.EmployeeService;
import com.da.datn.service.RefreshTokenService;
import com.da.datn.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

import static com.da.datn.utils.UserUtils.getCurrentUser;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1")
public class UserController {
    final RefreshTokenService refreshTokenService;
    final UserService userService;
    final AdminService adminService;
    final UserRepository userRepository;
    final EmployeeService employeeService;


    @PostMapping("/public/register")
    public ResponseEntity<?> registerAdmin(@RequestBody UserDTO userDTO) {
        return ResponseEntity.ok(adminService.register(userDTO));
    }

    @GetMapping("/public/alive")
    public String alive() {
        return "alive";
    }

    @PutMapping("user")
    public ResponseEntity<?> updateUser(@RequestBody UserDTO userDTO) {
        return new ResponseEntity<>(userService.updateUser(userDTO), HttpStatus.OK);
    }

    @GetMapping("/public/abc")
    public String registerAbc() {
        return getCurrentUser().getUsername();
    }

    @GetMapping("/user/{username}")
    public ResponseEntity<?> information(@PathVariable(value = "username") String username) {
        return ResponseEntity.ok(userService.findByUserName(username));
    }

    @PostMapping("/register")
    public ResponseEntity<?> register(@RequestBody UserDTO userDTO) {
        return ResponseEntity.ok(adminService.register(userDTO));
    }

    @PostMapping("/refreshToken")
    public ResponseEntity<?> refreshToken(@RequestBody RefreshTokenRequest request) {
        Map<String, String> token = refreshTokenService.refreshToken(request.getUsername(), request);
        return new ResponseEntity<>(ResponseDTO.builder().data(token).build(), HttpStatus.OK);
    }

    @PostMapping("/change-password")
    public ResponseEntity<?> changePassword(@RequestBody ChangePasswordDTO changePasswordDTO) {
        userService.changePassword(changePasswordDTO);
        return ResponseEntity.ok().build();
    }

    @PostMapping("/active")
    public ResponseEntity<?> active(@RequestBody UserDTO userDTO) {
        Users users = userRepository.findByUsername(userDTO.getUsername());
        users.setActive(!userDTO.getActive());
        userRepository.save(users);
        return ResponseEntity.ok().build();
    }

    @PostMapping("/forgot-password")
    public ResponseEntity<?> forgotPassword(@RequestParam String email) {
        userService.forgotPassword(email);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/accept-by")
    public ResponseEntity<?> getAcceptBy() {
        return new ResponseEntity<>(employeeService.getAcceptBy(), HttpStatus.OK);
    }

    @GetMapping("/public/check-user")
    public ResponseEntity<?> checkUser(@RequestBody UserDTO userDTO) {
        return new ResponseEntity<>(employeeService.getAcceptBy(), HttpStatus.OK);
    }
}
