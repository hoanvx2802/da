package com.da.datn.controller;

import com.da.datn.dto.InforWaterAndElecDTO;
import com.da.datn.dto.PageDTO;
import com.da.datn.dto.WAEPageDTO;
import com.da.datn.dto.req.InforWAE;
import com.da.datn.entity.WaterAndElectric;
import com.da.datn.service.WaterAndElectricService;
import com.da.datn.utils.PageUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1")
public class WaterAndElectricController {

    final WaterAndElectricService waterAndElectricService;

    @PostMapping("/fetch-wae")
    public ResponseEntity<?> fetchData(@RequestParam(required = false, name = "month") String month,
                                       @RequestParam(required = false, name = "year") String year) {
        return new ResponseEntity<>(waterAndElectricService.fetch(year, month), HttpStatus.OK);
    }

    @PostMapping("/utilities")
    public ResponseEntity<?> createWaterAndElectric(@RequestBody InforWaterAndElecDTO inforWaterAndElecDTO) {
        return new ResponseEntity<>(waterAndElectricService.inputInfor(inforWaterAndElecDTO), HttpStatus.OK);
    }

    @GetMapping("/utilities")
    public ResponseEntity<WAEPageDTO<InforWaterAndElecDTO>> getWaterAndElectric(@RequestParam(required = false, defaultValue = "1") Integer currentPage,
                                                                                @RequestParam(required = false, defaultValue = "10") Integer limit,
                                                                                @RequestParam(defaultValue = "id-ASC") String sort,
                                                                                @RequestParam(defaultValue = "") String year,
                                                                                @RequestParam(defaultValue = "") String month,
                                                                                @RequestParam(defaultValue = "%") String keyword) {
        Pageable pageable = PageUtils.getPageable(currentPage, limit, sort);
        return new ResponseEntity<>(waterAndElectricService.getInforWaterAndElec(pageable, keyword, year, month), HttpStatus.OK);
    }

    @PutMapping("/utilities")
    public ResponseEntity<?> updateWaterAndElectric(@RequestBody InforWAE inforWaterAndElecDTO) {
        return new ResponseEntity<>(waterAndElectricService.updateWaterAndElectric(inforWaterAndElecDTO), HttpStatus.OK);
    }

    @DeleteMapping("/utilities/{id}")
    public ResponseEntity<?> deleteWaterAndElectric(@PathVariable Long id) {
        return new ResponseEntity<>(waterAndElectricService.deleteWaterAndElectric(id), HttpStatus.OK);
    }
}
