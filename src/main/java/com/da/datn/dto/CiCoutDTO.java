package com.da.datn.dto;

import com.da.datn.entity.Student;
import com.da.datn.enums.StatusCiCout;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class CiCoutDTO {
    String studentName;
    String image;
    LocalDateTime timeCheck;
    StatusCiCout status;
}
