package com.da.datn.dto;

import com.da.datn.entity.Employee;
import com.da.datn.entity.Room;
import com.da.datn.entity.Student;
import com.da.datn.enums.StatusContract;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ContractDTO {
    Long id;
    String studentName;
    String employeeName;
    Long roomId;
    Long roomNumber;
    LocalDate createAt;
    LocalDate startAt;
    LocalDate endAt;
    StatusContract status;
    String studentFullName;
    String employeeFullName;
}
