package com.da.datn.dto;

import com.da.datn.dto.common.UserDTO;
import com.da.datn.entity.Employee;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class EmployeeDTO extends UserDTO {

    private String employeeCode;
    private LocalDate onBoard;
    private LocalDate offBoard;

    public EmployeeDTO(Employee employee) {
        super(employee);
        this.employeeCode = employee.getEmployeeCode();
        this.onBoard = employee.getOnBoard();
        this.offBoard = employee.getOffBoard();
    }
}
