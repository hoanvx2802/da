package com.da.datn.dto;

import com.da.datn.entity.WaterAndElectric;
import com.da.datn.enums.StatusWaterAndElectric;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class InforWaterAndElecDTO {
    Long id;
    String employeeName; //
    Long roomId;
    String number; // roomNumber
    Long roomPrice;
    Integer numberOfRoom;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "MM/yyyy")
    @DateTimeFormat(pattern = "MM/yyyy")
    LocalDate monthApply;
    Long startWaterNumber;
    Long endWaterNumber;
    String waterCode;
    Long startElectricNumber;
    Long endElectricNumber;
    String electricMeterCode;
    Long waterPrice;
    Long electricPrice;
    StatusWaterAndElectric status;

    public InforWaterAndElecDTO(WaterAndElectric waterAndElectric) {
        this.id = waterAndElectric.getId();
        this.employeeName = waterAndElectric.getEmployeeName();
        this.roomId = waterAndElectric.getRoomId();
        this.monthApply = waterAndElectric.getMonthApply();
        this.startWaterNumber = waterAndElectric.getStartWaterNumber();
        this.endWaterNumber = waterAndElectric.getEndWaterNumber();
        this.waterCode = waterAndElectric.getWaterCode();
        this.startElectricNumber = waterAndElectric.getStartElectricNumber();
        this.endElectricNumber = waterAndElectric.getEndElectricNumber();
        this.electricMeterCode = waterAndElectric.getElectricMeterCode();
        this.status = waterAndElectric.getStatus();
        this.waterPrice = waterAndElectric.getWaterPrice();
        this.electricPrice = waterAndElectric.getElectricPrice();
    }
}
