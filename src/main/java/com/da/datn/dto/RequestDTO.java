package com.da.datn.dto;

import com.da.datn.entity.Request;
import com.da.datn.enums.StatusRequest;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class RequestDTO {
    Long id;
    String description;
    String createName;
    LocalDate createAt;
    LocalDate updateAt;
    StatusRequest status;
    String userChange;
    String acceptBy;
    String comment;
    String createFullName;
    String acceptByFull;
    String userChangeFull;

    public RequestDTO(Request request) {
        this.id = request.getId();
        this.description = request.getDescription();
        this.createName = request.getCreateName();
        this.createAt = request.getCreateAt();
        this.updateAt = request.getUpdateAt();
        this.status = request.getStatus();
        this.userChange = request.getUserChange();
        this.acceptBy = request.getAcceptBy();
        this.comment = request.getComment();
    }
}
