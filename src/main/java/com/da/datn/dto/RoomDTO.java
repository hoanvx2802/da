package com.da.datn.dto;

import com.da.datn.enums.StatusRoom;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class RoomDTO {
    Long id;
    String number;
    String building;
    Long capacity;
    Integer currentQuantity;
    StatusRoom status;
    String description;
    Long price;
    Long waterPrice;
    Long electricPrice;
    Boolean available;
}
