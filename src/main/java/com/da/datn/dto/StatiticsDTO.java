package com.da.datn.dto;

import com.da.datn.entity.WaterAndElectric;
import lombok.*;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class StatiticsDTO {
    List<WaterAndElectric> data;
    Long totalWaterNumber;
    Long totalElectricNumber;
}
