package com.da.datn.dto;

import com.da.datn.dto.common.UserDTO;
import com.da.datn.entity.Student;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class StudentDTO extends UserDTO {
    String studentCode;
    String className;
    String department;
    String status;
    String emergencyNumber;
    Long roomId;

    public StudentDTO(Student student) {
        super(student);
        this.studentCode = student.getStudentCode();
        this.className = student.getClassName();
        this.department = student.getDepartment();
        this.status = student.getRoomId() == null ? "Đã xếp phòng" : "Chưa xếp phòng";
        this.emergencyNumber = student.getEmergencyNumber();
        this.roomId = (student.getRoomId() == null) ? null : student.getRoomId();
    }
}
