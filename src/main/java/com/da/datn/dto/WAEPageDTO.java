package com.da.datn.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class WAEPageDTO<T> {
    private List<T> data;
    private int currentPage;
    private int limit;
    private long totalPages;
    private long totalElements;
    long totalWater;
    long totalElectric;
}
