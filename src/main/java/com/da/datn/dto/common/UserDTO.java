package com.da.datn.dto.common;

import com.da.datn.entity.Employee;
import com.da.datn.entity.Student;
import com.da.datn.enums.Role;
import jakarta.persistence.*;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class UserDTO {
    private Long id;
    @NotBlank
    @Column(unique = true, nullable = false)
    private String username;
    @Column(nullable = false)
    private String password;
    @NotBlank
    @Email
    @Column(unique = true, nullable = false)
    private String email;
    private Boolean sex;
    @Enumerated(EnumType.STRING)
    private Role role;
    private String fullName;
    private String imageUrl;
    private String phoneNumber;
    private Boolean active;
    private String citizenIdentificationCard;
    private String address;

    public UserDTO(Student student) {
        this.id = student.getId();
        this.username = student.getUsername();
        this.email = student.getEmail();
        this.sex = student.getGender();
        this.role = student.getRole();
        this.fullName = student.getFullName();
        this.imageUrl = student.getImageUrl();
        this.phoneNumber = student.getPhoneNumber();
        this.active = student.getActive();
        this.citizenIdentificationCard = student.getCitizenIdentificationCard();
        this.address = student.getAddress();
    }

    public UserDTO(Employee employee) {
        this.id = employee.getId();
        this.username = employee.getUsername();
        this.email = employee.getEmail();
        this.sex = employee.getGender();
        this.role = employee.getRole();
        this.fullName = employee.getFullName();
        this.imageUrl = employee.getImageUrl();
        this.phoneNumber = employee.getPhoneNumber();
        this.active = employee.getActive();
        this.citizenIdentificationCard = employee.getCitizenIdentificationCard();
        this.address = employee.getAddress();
    }

}
