package com.da.datn.dto.req;

import com.da.datn.enums.StatusContract;
import com.da.datn.enums.StatusRequest;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class AcceptOrRejectRequestDTO {
    Long id;
    StatusRequest status;
    String comment;
}
