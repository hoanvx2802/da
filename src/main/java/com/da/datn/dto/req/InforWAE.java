package com.da.datn.dto.req;

import com.da.datn.enums.StatusWaterAndElectric;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class InforWAE {
    Long id;
    Long startWaterNumber;
    Long endWaterNumber;
    String waterCode;
    Long startElectricNumber;
    Long endElectricNumber;
    String electricMeterCode;
    StatusWaterAndElectric status;
}