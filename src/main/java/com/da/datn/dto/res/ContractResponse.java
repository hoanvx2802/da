package com.da.datn.dto.res;

import com.da.datn.entity.Contract;
import com.da.datn.enums.StatusContract;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ContractResponse {
    Long id;
    String studentFullName;
    String employeeFullName;
    String studentName;
    String employeeName;
    String roomNumber;
    Long roomId;
    LocalDate createAt;
    LocalDate updateAt;
    LocalDate startAt;
    LocalDate endAt;
    StatusContract status;
    String changeName;
    String changeFullName;
    String acceptBy;
    String acceptByFull;
    Boolean deleteFlag;
    String comment;
    public ContractResponse(Contract contract, String studentFullName, String employeeFullName, String roomNumber) {
        this.studentFullName = studentFullName;
        this.employeeFullName = employeeFullName;
        this.roomNumber = roomNumber;
        this.id = contract.getId();
        this.roomId = contract.getRoomId();
        this.studentName = contract.getStudentName();
        this.employeeName = contract.getEmployeeName();
        this.createAt = contract.getCreateAt();
        this.updateAt = contract.getUpdateAt();
        this.startAt = contract.getStartAt();
        this.endAt = contract.getEndAt();
        this.status = contract.getStatus();
        this.changeName = contract.getChangeName();
        this.acceptBy = contract.getAcceptBy();
        this.deleteFlag = contract.getDeleteFlag();
        this.comment = contract.getComment();
    }
}
