package com.da.datn.entity;

import com.da.datn.enums.StatusCiCout;
import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CheckInCheckout {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    String studentName;
    Long studentId;
    String image;
    LocalDateTime timeCheck;
    @Enumerated(EnumType.STRING)
    StatusCiCout status;
}
