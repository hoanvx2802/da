package com.da.datn.entity;

import com.da.datn.enums.StatusRequest;
import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.SuperBuilder;

import java.time.LocalDate;

@Entity
@Getter
@Setter
@NoArgsConstructor
@SuperBuilder
public class Request {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    String description;
    String createName;
    Long roomId;
    String roomNumber;
    LocalDate createAt;
    LocalDate updateAt;
    String acceptBy;
    String userChange;
    @Enumerated(EnumType.STRING)
    StatusRequest status;
    String comment;
}
