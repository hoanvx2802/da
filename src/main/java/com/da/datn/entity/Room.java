package com.da.datn.entity;

import com.da.datn.enums.StatusRoom;
import jakarta.persistence.*;
import lombok.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "room", uniqueConstraints = @UniqueConstraint(columnNames = {"number", "building"}))
public class Room {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    String number;
    String building;
    Long capacity;
    @Enumerated(EnumType.STRING)
    StatusRoom status;
    String description;
    Long price;
    Long waterPrice;
    Long electricPrice;
    Boolean available;
    String changeName;
}
