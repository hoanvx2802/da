package com.da.datn.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Entity
@Getter
@Setter
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class Student extends Users{
    @Column(unique = true)
    String studentCode;
    String className;
    String department;
    String status;
    String emergencyNumber;
    Long roomId;
    String urlQr;

}
