package com.da.datn.entity;

import com.da.datn.enums.Role;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import lombok.*;
import lombok.experimental.SuperBuilder;

import java.time.LocalDate;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public class Users {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    @NotBlank
    @Column(unique = true, nullable = false)
    String username;
    @Column(nullable = false)
    @JsonIgnore
    String password;
    @NotBlank
    @Email
    @Column(unique = true, nullable = false)
    String email;
    Boolean gender;
    @Enumerated(EnumType.STRING)
    Role role;
    String fullName;
    String imageUrl;
    String phoneNumber;
    Boolean active;
    String address;
    LocalDate blockRequest;
    String citizenIdentificationCard;
    String changeName;

}
