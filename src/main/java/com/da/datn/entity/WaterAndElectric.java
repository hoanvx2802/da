package com.da.datn.entity;

import com.da.datn.enums.StatusWaterAndElectric;
import com.fasterxml.jackson.annotation.JsonFormat;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

@Entity
@Getter
@Setter
@NoArgsConstructor
@SuperBuilder
@Table(name = "water_and_electric", uniqueConstraints = @UniqueConstraint(columnNames = {"employeeName", "month_apply", "roomId"}))
public class WaterAndElectric {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    String description;
    String employeeName;
    Long roomId;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    LocalDate createAt;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "MM/yyyy")
    @DateTimeFormat(pattern = "MM/yyyy")
    LocalDate monthApply;

    Long startWaterNumber;
    Long endWaterNumber;
    String waterCode;
    Long startElectricNumber;
    Long endElectricNumber;
    String electricMeterCode;
    @Enumerated(EnumType.STRING)
    StatusWaterAndElectric status;
    Long waterPrice;
    Long electricPrice;
    String changeName;
}
