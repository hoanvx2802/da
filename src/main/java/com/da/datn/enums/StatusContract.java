package com.da.datn.enums;

import lombok.Getter;

@Getter
public enum StatusContract {
    APPROVED(2),
    REJECTED(2),
    CANCELED(3),
    PENDING(1),
    TIME_OUT(3);

    final Integer level;
    StatusContract(Integer level) {
        this.level = level;
    }

}
