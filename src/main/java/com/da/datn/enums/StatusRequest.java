package com.da.datn.enums;

public enum StatusRequest {
    APPROVED(2),
    REJECTED(2),
    PENDING(1),
    DELETED(3),
    CANCELED(3);

    final Integer level;
    StatusRequest(Integer level) {
        this.level = level;
    }

    public Integer getLevel() {
        return level;
    }
}
