package com.da.datn.enums;

public enum StatusWaterAndElectric {
    CREATED(1),
    TIME_OUT(3)
    ;

    final Integer level;
    StatusWaterAndElectric(Integer level) {
        this.level = level;
    }

    public Integer getLevel() {
        return level;
    }
}
