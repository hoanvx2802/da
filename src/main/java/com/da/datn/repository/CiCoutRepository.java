package com.da.datn.repository;

import com.da.datn.dto.PageDTO;
import com.da.datn.entity.CheckInCheckout;
import com.da.datn.entity.Contract;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Set;

public interface CiCoutRepository extends JpaRepository<CheckInCheckout, Long>, JpaSpecificationExecutor<CheckInCheckout> {

    @Query("SELECT c FROM CheckInCheckout c JOIN Student s on s.id = c.studentId WHERE c.status in (:status) AND s.fullName LIKE CONCAT('%', :fullName, '%')")
    Page<CheckInCheckout> findAllByStatusAndStudentFullName(Set<String> status, String fullName, Pageable pageable);
}
