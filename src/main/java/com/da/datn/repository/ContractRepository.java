package com.da.datn.repository;

import com.da.datn.dto.res.ContractResponse;
import com.da.datn.entity.Contract;
import com.da.datn.enums.StatusContract;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Set;

public interface ContractRepository extends JpaRepository<Contract, Long>, JpaSpecificationExecutor<Contract> {
    Contract findByStudentNameAndRoomIdAndStatusIn(String studentName, Long roomId, Set<StatusContract> status);

    List<Contract> findAllByStatus(StatusContract status);
    List<Contract> findByRoomId(Long roomId);
    Contract findByStudentNameAndStatusIn(String username, Set<StatusContract> status);
    List<Contract> findAllByRoomIdAndStatus(Long roomId, StatusContract status);
    @Query("SELECT new com.da.datn.dto.res.ContractResponse(c, s.fullName, e.fullName, r.number) FROM Contract c " +
            "join Student s on s.username = c.studentName " +
            "JOIN Room r on r.id = c.roomId " +
            "JOIN Users e on e.username = c.employeeName " +
            "WHERE c.studentName like :keyword " +
            "AND c.status in :status AND c.deleteFlag = :deleteFlag ORDER BY c.deleteFlag, c.createAt DESC ")
    Page<ContractResponse> findAllByCondition(String keyword, Boolean deleteFlag, Set<String> status, Pageable pageable);

    @Query("SELECT new com.da.datn.dto.res.ContractResponse(c, s.fullName, e.fullName, r.number) FROM Contract c " +
            "join Student s on s.username = c.studentName " +
            "JOIN Room r on r.id = c.roomId " +
            "JOIN Users e on e.username = c.employeeName " +
            "WHERE (c.studentName like :keyword or c.employeeName like :keyword or s.fullName like :keyword or e.fullName like :keyword) " +
            "AND c.status in (:status) ORDER BY c.deleteFlag, c.createAt DESC ")
    Page<ContractResponse> findAllByConditionRoleAdmin(@Param("keyword") String keyword, @Param("status") Set<String> status, Pageable pageable);
}
