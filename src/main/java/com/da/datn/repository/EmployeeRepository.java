package com.da.datn.repository;

import com.da.datn.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long>, JpaSpecificationExecutor<Employee> {
    Optional<Employee> findByUsername(String username);

    Employee findByEmployeeCode(String employeeCode);
    Optional<Employee> findById(Long id);
    List<Employee> findAllByActive(Boolean active);
}
