package com.da.datn.repository;

import com.da.datn.entity.Room;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface RoomRepository extends JpaRepository<Room, Long>, JpaSpecificationExecutor<Room> {
    Room findByNumberAndBuilding(String roomNumber, String building);
    List<Room> findAllByAvailable(Boolean available);

    @Query("SELECT r FROM Room r JOIN Student s ON s.roomId = r.id WHERE r.number = ?1 AND s.username = ?2")
    Optional<Room> findByNumberAndStudentName(String roomNumber, String username);
}
