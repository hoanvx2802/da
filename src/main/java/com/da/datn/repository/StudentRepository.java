package com.da.datn.repository;

import com.da.datn.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface StudentRepository extends JpaRepository<Student, Long>, JpaSpecificationExecutor<Student> {

    Student findByStudentCode(String studentCode);
    List<Student> findAllByFullNameLike(String keyword);
    List<Student> findAllByRoomId(Long id);
    Optional<Student> findByUsername(String username);
    Student findByEmail(String email);

}
