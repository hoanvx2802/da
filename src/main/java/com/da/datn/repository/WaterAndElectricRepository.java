package com.da.datn.repository;

import com.da.datn.entity.WaterAndElectric;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface WaterAndElectricRepository extends JpaRepository<WaterAndElectric, Long>, JpaSpecificationExecutor<WaterAndElectric> {
    Optional<WaterAndElectric> findByRoomId(Long id);

    @Query("SELECT w FROM WaterAndElectric w JOIN Room r ON r.id = w.roomId WHERE r.available = :available")
    List<WaterAndElectric> findAllByRoom_Available(Boolean available);
    @Query("SELECT w  " +
            "FROM WaterAndElectric w " +
            "JOIN Room r " +
            "ON r.id = w.roomId " +
            "WHERE w.monthApply BETWEEN :start AND :end " +
            "AND r.number LIKE :keyword " +
            "AND r.available = true")
    Page<WaterAndElectric> getInforWaterAndElec(String keyword, LocalDate start, LocalDate end, Pageable pageable);

    List<WaterAndElectric> findAllByMonthApplyBetween(LocalDate start, LocalDate end);

    WaterAndElectric findByRoomIdAndMonthApplyBetween(Long id, LocalDate start, LocalDate end);
}
