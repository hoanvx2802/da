package com.da.datn.service;

import com.da.datn.dto.EmployeeDTO;
import com.da.datn.dto.StudentDTO;
import com.da.datn.dto.common.UserDTO;
import com.da.datn.entity.Admin;
import com.da.datn.entity.Employee;
import com.da.datn.entity.Student;

public interface AdminService {
    Admin register(UserDTO userDTO);

//    Student createStudent(StudentDTO studentDTO);
//
//    Employee createStaff(EmployeeDTO staffDTO);
//
//    void removeStudent(Long id);
//
//    void removeStaff(Long id);
}
