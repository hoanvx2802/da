package com.da.datn.service;

import com.da.datn.entity.*;
import com.da.datn.enums.Role;
import com.da.datn.enums.StatusContract;
import com.da.datn.enums.StatusRoom;
import com.da.datn.enums.StatusWaterAndElectric;
import com.da.datn.repository.*;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Component
@RequiredArgsConstructor
public class AutoDB implements ApplicationContextAware {

    final RoomRepository roomRepository;
    final StudentRepository studentRepository;
    final SystemConfigRepository systemConfigRepository;
    final ContractRepository contractRepository;
    final EmployeeRepository employeeRepository;
    final UserRepository userRepository;
    final WaterAndElectricRepository waterAndElectricRepository;
    final AdminRepository adminRepository;

    final PasswordEncoder passwordEncoder;

    @Getter
    private ApplicationContext context;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        List<SystemConfig> systemConfigs = systemConfigRepository.findAll();
        if (systemConfigs.isEmpty()) {
            createSystemDefault();
        }
        List<Room> rooms = roomRepository.findAll();
        List<Student> students = studentRepository.findAll();
        List<Contract> contracts = contractRepository.findAll();
        List<Employee> employees = employeeRepository.findAll();
        List<Users> users = userRepository.findAll();
        List<WaterAndElectric> waterAndElectrics = waterAndElectricRepository.findAll();
        if (rooms.isEmpty() || students.isEmpty()
                || contracts.isEmpty() || employees.isEmpty()
                || waterAndElectrics.isEmpty() || users.isEmpty()) {
            createDefault();
        }
    }

    private void createDefault() {
        // tạo tài khoản admin
        Admin admin = Admin.builder()
                .username("admin")
                .password(passwordEncoder.encode("1"))
                .email("admin@gmail.com")
                .gender(false)
                .role(Role.ADMIN)
                .fullName("Quản lý chung")
                .phoneNumber("0000000000")
                .active(true)
                .address("HN")
                .citizenIdentificationCard("11111781234")
                .build();
        adminRepository.save(admin);

        // tạo tài khoản cho nhân viên hành chính
        Employee employee1 = Employee.builder()
                .username("nhanvienhanhchinh1")
                .password(passwordEncoder.encode("1"))
                .email("nhanvienhanhchinh1@gmail.com")
                .gender(true)
                .role(Role.ADMINISTRATIVE)
                .fullName("Quản lý hành chính")
                .phoneNumber("012343123")
                .active(true)
                .address("HN")
                .citizenIdentificationCard("22222781234")
                .employeeCode("NVHC")
                .onBoard(LocalDate.now())
                .offBoard(LocalDate.now().plusMonths(1))
                .build();
        // tạo tài khoản cho nhân viên tài chính
        Employee employee2 = Employee.builder()
                .username("nhanvientaichinh1")
                .password(passwordEncoder.encode("1"))
                .email("nhanvientaichinh1@gmail.com")
                .gender(true)
                .role(Role.MANAGEMENT_FINANCIAL)
                .fullName("Quản lý tài chính")
                .phoneNumber("0123433333")
                .active(true)
                .address("HN")
                .citizenIdentificationCard("33333781234")
                .employeeCode("NVTC")
                .onBoard(LocalDate.now())
                .offBoard(LocalDate.now().plusMonths(1))
                .build();
        employeeRepository.saveAll(List.of(employee1, employee2));

        // tạo phòng
        Room room = Room.builder()
                .number("101")
                .building("A")
                .capacity(2L)
                .status(StatusRoom.CHUA_CO_NGUOI)
                .description("Phòng 1 của tòa A")
                .price(100000L)
                .available(true)
                .build();
        roomRepository.save(room);
        // Tạo nhân viên
        Student student = Student.builder()
                .username("sinhvien1")
                .password(passwordEncoder.encode("1"))
                .email("sinhvien1@gmail.com")
                .gender(true)
                .role(Role.STUDENT)
                .fullName("Sinh Viên 1")
                .phoneNumber("0111111111")
                .active(true)
                .address("HCM")
                .citizenIdentificationCard("123456781234")
                .studentCode("11164")
                .className("CS1")
                .department("CNTT")
                .emergencyNumber("0111100000")
                .roomId(room.getId())
                .build();
        studentRepository.save(student);

        /* Tạo hợp đồng cho nhân viên và phòng bên(chưa đủ người) trên vừa tạo
        * nhưng chưa được admin accept
        */
        Contract contract = Contract.builder()
                .studentName(student.getUsername())
                .employeeName(employee1.getUsername())
                .roomId(room.getId())
                .createAt(LocalDate.now())
                .startAt(LocalDate.now())
                .endAt(LocalDate.now().plusMonths(1))
                .status(StatusContract.PENDING)
                .deleteFlag(false)
                .build();
        contractRepository.save(contract);

        Room room1 = Room.builder()
                .number("102")
                .building("A")
                .capacity(2L)
                .status(StatusRoom.DU_NGUOI)
                .description("Phòng 2 của tòa A")
                .price(150000L)
                .available(true)
                .build();
        roomRepository.save(room1);
        Student student2 = Student.builder()
                .username("sinhvien2")
                .password(passwordEncoder.encode("1"))
                .email("sinhvien2@gmail.com")
                .gender(true)
                .role(Role.STUDENT)
                .fullName("Sinh Viên 2")
                .phoneNumber("0222222222")
                .active(true)
                .address("HCM")
                .citizenIdentificationCard("123412341234")
                .studentCode("22264")
                .className("CS1")
                .department("CNTT")
                .emergencyNumber("0222200000")
                .roomId(room1.getId())
                .build();
        Student student3 = Student.builder()
                .username("sinhvien3")
                .password(passwordEncoder.encode("1"))
                .email("sinhvien3@gmail.com")
                .gender(true)
                .role(Role.STUDENT)
                .fullName("Sinh Viên 3")
                .phoneNumber("0333333333")
                .active(true)
                .address("HN")
                .citizenIdentificationCard("333344441234")
                .studentCode("33364")
                .className("CS2")
                .department("CNTT")
                .emergencyNumber("0333300000")
                .roomId(room1.getId())
                .build();
        studentRepository.saveAll(List.of(student2, student3));

        /* Tạo hợp đồng cho nhân viên và phòng bên trên(đủ người) vừa tạo
         * đã được admin accept
         */
        Contract contract2 = Contract.builder()
                .studentName(student2.getUsername())
                .employeeName(employee1.getUsername())
                .roomId(room1.getId())
                .createAt(LocalDate.now())
                .startAt(LocalDate.now())
                .endAt(LocalDate.now().plusMonths(1))
                .status(StatusContract.APPROVED)
                .acceptBy(admin.getUsername())
                .comment("ok")
                .deleteFlag(false)
                .build();
        Contract contract3 = Contract.builder()
                .studentName(student3.getUsername())
                .employeeName(employee1.getUsername())
                .roomId(room1.getId())
                .createAt(LocalDate.now())
                .startAt(LocalDate.now())
                .endAt(LocalDate.now().plusMonths(1))
                .status(StatusContract.APPROVED)
                .acceptBy(admin.getUsername())
                .comment("ok")
                .deleteFlag(false)
                .build();
        contractRepository.saveAll(List.of(contract2, contract3));

        // Tạo thêm 1 vài phòng trống
        Room room2 = Room.builder()
                .number("103")
                .building("A")
                .capacity(2L)
                .status(StatusRoom.CHUA_CO_NGUOI)
                .description("Phòng 3 của tòa A")
                .price(200000L)
                .available(true)
                .build();
        Room room3 = Room.builder()
                .number("101")
                .building("B")
                .capacity(2L)
                .status(StatusRoom.CHUA_CO_NGUOI)
                .description("Phòng 1 của tòa B")
                .price(200000L)
                .available(true)
                .build();
        Room room4 = Room.builder()
                .number("102")
                .building("B")
                .capacity(4L)
                .status(StatusRoom.CHUA_CO_NGUOI)
                .description("Phòng 2 của tòa B")
                .price(400000L)
                .available(true)
                .build();
        roomRepository.saveAll(List.of(room2, room3, room4));

        // Tạo thông số điện nước
        LocalDate date = LocalDate.now();
        WaterAndElectric waterAndElectric = WaterAndElectric.builder()
                .description("Điện nước phòng 1 tòa A tháng " + date.getMonth())
                .employeeName(employee2.getUsername())
                .roomId(room.getId())
                .createAt(date)
                .monthApply(date)
                .startWaterNumber(0L)
                .endWaterNumber(10L)
                .waterCode("W101")
                .startElectricNumber(0L)
                .endElectricNumber(20L)
                .electricMeterCode("E101")
                .status(StatusWaterAndElectric.CREATED)
                .waterPrice(100000L)
                .electricPrice(20000L)
                .build();
        WaterAndElectric waterAndElectric1 = WaterAndElectric.builder()
                .description("Điện nước phòng 2 tòa A tháng " + date.getMonth())
                .employeeName(employee2.getUsername())
                .roomId(room1.getId())
                .createAt(date)
                .monthApply(date)
                .startWaterNumber(0L)
                .endWaterNumber(15L)
                .waterCode("W102")
                .startElectricNumber(0L)
                .endElectricNumber(10L)
                .electricMeterCode("E102")
                .status(StatusWaterAndElectric.CREATED)
                .waterPrice(150000L)
                .electricPrice(10000L)
                .build();
        waterAndElectricRepository.saveAll(List.of(waterAndElectric, waterAndElectric1));
    }

    private void createSystemDefault() {
        SystemConfig systemConfigWPrice = SystemConfig.builder().keyword("W_PRICE").value("10000").build();
        SystemConfig systemConfigEPrice = SystemConfig.builder().keyword("E_PRICE").value("1000").build();
        systemConfigRepository.saveAll(List.of(systemConfigWPrice, systemConfigEPrice));
    }
}
