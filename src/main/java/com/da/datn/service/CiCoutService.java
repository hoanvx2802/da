package com.da.datn.service;

import com.da.datn.dto.PageDTO;
import com.da.datn.entity.CheckInCheckout;
import org.springframework.data.domain.Pageable;

import java.util.Set;

public interface CiCoutService {
    PageDTO<CheckInCheckout> getAll(Pageable pageable, String keyword, Set<String> status);
}
