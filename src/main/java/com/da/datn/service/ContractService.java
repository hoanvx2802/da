package com.da.datn.service;

import com.da.datn.dto.ContractDTO;
import com.da.datn.dto.PageDTO;
import com.da.datn.dto.req.AcceptOrRejectContractDTO;
import com.da.datn.dto.req.AcceptOrRejectRequestDTO;
import com.da.datn.dto.res.ContractResponse;
import com.da.datn.entity.Contract;
import com.da.datn.enums.StatusContract;
import org.springframework.data.domain.Pageable;

import java.util.Set;

public interface ContractService {
    Contract create(ContractDTO contractDTO);

    Contract update(ContractDTO contractDTO);

    void delete(Long id);

    PageDTO<ContractResponse> findAllContract(Pageable pageable, String keyword, Set<String> status);
    PageDTO<ContractResponse> findAllManagerContract(Pageable pageable, String keyword, Set<String> status, Boolean isAccept);

    Contract accpectOrRejectContract(AcceptOrRejectContractDTO dto);

    Contract extendContract(ContractDTO contractDTO);
}
