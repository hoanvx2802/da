package com.da.datn.service;

import com.da.datn.dto.EmployeeDTO;
import com.da.datn.dto.PageDTO;
import com.da.datn.entity.Employee;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface EmployeeService {

    PageDTO<EmployeeDTO> getAllEmployee(Pageable pageable, String keyword);

    Employee createEmployee(EmployeeDTO employeeDTO);

    void removeEmployee(Long id);

    Employee updateEmployee(EmployeeDTO employeeDTO);

    List getAcceptBy();
}
