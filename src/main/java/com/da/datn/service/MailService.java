package com.da.datn.service;

public interface MailService {
    void sendMail(String email);
    void sendForgotPasswordMail(String email, String newPassword);

}
