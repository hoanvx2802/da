package com.da.datn.service;

import com.da.datn.dto.req.RefreshTokenRequest;

import java.util.Map;

public interface RefreshTokenService {
    Map<String, String> refreshToken(String username, RefreshTokenRequest request);
}
