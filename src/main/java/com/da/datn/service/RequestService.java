package com.da.datn.service;

import com.da.datn.dto.PageDTO;
import com.da.datn.dto.RequestDTO;
import com.da.datn.dto.req.AcceptOrRejectRequestDTO;
import com.da.datn.entity.Request;
import org.springframework.data.domain.Pageable;

import java.time.LocalDate;
import java.util.Set;

public interface RequestService {
    Request create(RequestDTO requestDTO);
    Request update(RequestDTO requestDTO);
    void delete(Long id);
    PageDTO<RequestDTO> findAllManagerRequest(Pageable pageable, String keyword, Set<String> status, Boolean isAccept, LocalDate from, LocalDate to);
    PageDTO<RequestDTO> findAllRequest(Pageable pageable, String keyword, Set<String> status, LocalDate from, LocalDate to);
    RequestDTO acceptOrRejectRequest(AcceptOrRejectRequestDTO acceptOrRejectRequestDTO);

}
