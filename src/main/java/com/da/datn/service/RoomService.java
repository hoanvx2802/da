package com.da.datn.service;

import com.da.datn.dto.PageDTO;
import com.da.datn.dto.RoomDTO;
import com.da.datn.entity.Room;
import org.springframework.data.domain.Pageable;

import java.util.Set;

public interface RoomService {

    Room createRoom(RoomDTO roomDTO);
    Room updateRoom(RoomDTO roomDTO);
    PageDTO<RoomDTO> getAllRooms(Pageable pageable, Set<String> status, String building, String roomNumber, Boolean isContract);
    void deleteRoom(Long id);
}
