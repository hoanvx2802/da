package com.da.datn.service;

import com.da.datn.dto.PageDTO;
import com.da.datn.dto.StatiticsDTO;

import java.time.LocalDate;
import java.util.List;

public interface StatiticsService {
    String calcBillRoom();
    StatiticsDTO getStatiticsForMonth(String month);
    String calcBillOneRoom(Long idRoom, Long waterPrice, Long electricPrice);
}
