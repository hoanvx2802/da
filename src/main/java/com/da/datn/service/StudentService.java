package com.da.datn.service;

import com.da.datn.dto.PageDTO;
import com.da.datn.dto.StudentDTO;
import com.da.datn.entity.Student;
import org.springframework.data.domain.Pageable;

public interface StudentService {
    PageDTO<StudentDTO> getAllStudents(Pageable pageable, String keyword, Boolean isContract);

    Student createStudent(StudentDTO studentDTO);

    void removeStudent(Long id);

    Student updateStudent(StudentDTO studentDTO);
}
