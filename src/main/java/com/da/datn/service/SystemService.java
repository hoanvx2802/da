package com.da.datn.service;

import com.da.datn.dto.PageDTO;
import com.da.datn.entity.SystemConfig;
import org.springframework.data.domain.Pageable;

public interface SystemService {
    SystemConfig addSystemConfig(String keyword, String value);

    SystemConfig updateSystemConfig(String keyword, String value);

    void cronSendBill();

    PageDTO<SystemConfig> findAllSystem(Pageable pageable, String keyword);
}
