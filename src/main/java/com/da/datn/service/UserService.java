package com.da.datn.service;

import com.da.datn.dto.common.UserDTO;
import com.da.datn.dto.req.ChangePasswordDTO;
import com.da.datn.entity.Users;
import org.springframework.web.multipart.MultipartFile;

public interface UserService {

    Users findByUserName(String username);

    String roleUser(String username);
    void changePassword(ChangePasswordDTO changePasswordDTO);

    void forgotPassword(String email);

    Users updateUser(UserDTO userDTO);
}
