package com.da.datn.service;

import com.da.datn.dto.InforWaterAndElecDTO;
import com.da.datn.dto.WAEPageDTO;
import com.da.datn.dto.req.InforWAE;
import org.springframework.data.domain.Pageable;

public interface WaterAndElectricService {

    String inputInfor(InforWaterAndElecDTO inforWaterAndElecDTO);

    String updateWaterAndElectric(InforWAE inforWaterAndElecDTO);

    String deleteWaterAndElectric(Long id);

    WAEPageDTO<InforWaterAndElecDTO> getInforWaterAndElec(Pageable pageable, String keyword, String year, String month);

    String fetch(String year, String month);
}
