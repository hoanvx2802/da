package com.da.datn.service.impl;

import com.da.datn.config.exception.ApplicationRuntimeException;
import com.da.datn.dto.EmployeeDTO;
import com.da.datn.dto.StudentDTO;
import com.da.datn.dto.common.UserDTO;
import com.da.datn.entity.Admin;
import com.da.datn.entity.Employee;
import com.da.datn.entity.Student;
import com.da.datn.enums.Role;
import com.da.datn.repository.AdminRepository;
import com.da.datn.repository.EmployeeRepository;
import com.da.datn.repository.StudentRepository;
import com.da.datn.service.AdminService;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class AdminServiceImpl implements AdminService {

    final AdminRepository adminRepository;
    final EmployeeRepository employeeRepository;
    final StudentRepository studentRepository;

    private final PasswordEncoder passwordEncoder;

    @Override
    public Admin register(UserDTO userDTO) {
        Admin admin = Admin.builder()
                .username(userDTO.getUsername())
                .email(userDTO.getEmail())
                .active(true)
                .role(Role.ADMIN)
                .password(passwordEncoder.encode(userDTO.getPassword()))
                .fullName(userDTO.getFullName())
                .phoneNumber(userDTO.getPhoneNumber())
                .gender(userDTO.getSex())
                .build();
        adminRepository.save(admin);
        return admin;
    }
}
