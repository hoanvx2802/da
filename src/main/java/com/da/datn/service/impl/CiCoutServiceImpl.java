package com.da.datn.service.impl;

import com.da.datn.config.exception.ApplicationRuntimeException;
import com.da.datn.dto.PageDTO;
import com.da.datn.dto.StudentDTO;
import com.da.datn.entity.CheckInCheckout;
import com.da.datn.entity.Student;
import com.da.datn.enums.StatusCiCout;
import com.da.datn.repository.CiCoutRepository;
import com.da.datn.repository.StudentRepository;
import com.da.datn.service.CiCoutService;
import lombok.RequiredArgsConstructor;
import org.hibernate.mapping.Join;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class CiCoutServiceImpl implements CiCoutService {
    final CiCoutRepository ciCoutRepository;
    final StudentRepository studentRepository;
    @Override
    public PageDTO<CheckInCheckout> getAll(Pageable pageable, String keyword, Set<String> status) {
        Set<String> statusList;
        if(status == null){
            statusList = Set.of(StatusCiCout.RA.toString(), StatusCiCout.VAO.toString());
        } else {
            statusList = status;
        }
        Page<CheckInCheckout> response = ciCoutRepository.findAllByStatusAndStudentFullName(statusList, keyword, pageable);

        return new PageDTO<>(
                response.getContent(),
                pageable.getPageNumber() + 1,
                pageable.getPageSize(),
                response.getTotalPages(),
                response.getTotalElements()
        );
    }
}
