package com.da.datn.service.impl;

import com.da.datn.config.exception.ApplicationRuntimeException;
import com.da.datn.dto.ContractDTO;
import com.da.datn.dto.PageDTO;
import com.da.datn.dto.req.AcceptOrRejectContractDTO;
import com.da.datn.dto.res.ContractResponse;
import com.da.datn.entity.Contract;
import com.da.datn.entity.Room;
import com.da.datn.entity.Student;
import com.da.datn.entity.Users;
import com.da.datn.enums.Role;
import com.da.datn.enums.StatusContract;
import com.da.datn.enums.StatusRoom;
import com.da.datn.repository.*;
import com.da.datn.service.ContractService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import static com.da.datn.utils.UserUtils.getCurrentUser;

@Slf4j
@Service
@RequiredArgsConstructor
public class ContractServiceImpl implements ContractService {

    final ContractRepository contractRepository;
    final StudentRepository studentRepository;
    final RoomRepository roomRepository;
    final EmployeeRepository employeeRepository;
    private final UserRepository userRepository;

    @Override
    public Contract create(ContractDTO contractDTO) {
        String username = getCurrentUser().getUsername();
        Student student = studentRepository.findByUsername(contractDTO.getStudentFullName()).orElseThrow(() -> new ApplicationRuntimeException("Không tìm thấy sinh viên"));
        Room room = roomRepository.findById(contractDTO.getRoomNumber()).orElseThrow(() -> new ApplicationRuntimeException("Không tìm thấy phòng hợp đồng"));
        Contract contract = contractRepository.findByStudentNameAndRoomIdAndStatusIn(student.getUsername(), room.getId(), Set.of(StatusContract.APPROVED, StatusContract.PENDING));
        if (contract != null) {
            throw new ApplicationRuntimeException("Hợp đồng đã được tạo trước đó. Vui lòng kiểm tra lại.");
        }
        contract = Contract.builder()
                .studentName(student.getUsername())
                .employeeName(username)
                .roomId(room.getId())
                .startAt(contractDTO.getStartAt())
                .endAt(contractDTO.getEndAt())
                .status(StatusContract.PENDING)
                .createAt(LocalDate.now())
                .deleteFlag(false)
                .build();
        return contractRepository.save(contract);
    }

    @Override
    public Contract update(ContractDTO contractDTO) {
        String username = getCurrentUser().getUsername();
        Student student = studentRepository.findByUsername(contractDTO.getStudentName()).orElseThrow(() -> new ApplicationRuntimeException("Không tìm thấy sinh viên"));
        Room room = roomRepository.findById(contractDTO.getRoomId()).orElseThrow(() -> new ApplicationRuntimeException("Không tìm thấy phòng hợp đồng"));
        Contract contract = contractRepository.findById(contractDTO.getId()).orElseThrow(() -> new ApplicationRuntimeException("Không tìm thấy hợp đồng"));
        contract.setStudentName(student.getUsername());
        contract.setEmployeeName(username);
        contract.setRoomId(room.getId());
        contract.setStartAt(contractDTO.getStartAt());
        contract.setEndAt(contractDTO.getEndAt());
        contract.setChangeName(username);
        contract.setUpdateAt(LocalDate.now());
        return contractRepository.save(contract);
    }

    @Override
    public void delete(Long id) {
        Contract contract = contractRepository.findById(id).orElseThrow(() -> new ApplicationRuntimeException("Không tìm thấy hợp đồng"));
        Role role = getCurrentUser().getRole();
        if (Role.ADMIN.equals(role)) {
            contractRepository.delete(contract);
        } else if (Role.ADMINISTRATIVE.equals(role)) {
            contract.setDeleteFlag(true);
            contractRepository.save(contract);
        }
    }

    @Override
    public PageDTO<ContractResponse> findAllManagerContract(Pageable pageable, String keyword, Set<String> status, Boolean isAccept) {
        Role role = getCurrentUser().getRole();
        keyword = "%" + keyword + "%";

        Page<ContractResponse> response;
        Set<String> finalStatus;
        if(status == null) {
            if (isAccept != null && isAccept) {
                finalStatus = Arrays.stream(StatusContract.values())
                        .filter(statusContract -> statusContract != StatusContract.PENDING)
                        .map(Enum::toString)
                        .collect(Collectors.toSet());
            } else {
                finalStatus = Set.of(StatusContract.PENDING.toString());
            }
        } else {
            finalStatus = status;
        }

        response = contractRepository.findAllByCondition(keyword, false, finalStatus, pageable);
        List<ContractResponse> list = response.getContent().stream().peek(temp -> {
            Users user = userRepository.findByUsername(temp.getChangeName());
            if (user != null) {
                temp.setChangeFullName(user.getFullName());
            }
            Users acceptBy = userRepository.findByUsername(temp.getAcceptBy());
            if (acceptBy != null) {
                temp.setAcceptByFull(acceptBy.getFullName());
            }
        }).toList();
        return new PageDTO<>(list, pageable.getPageNumber() + 1, pageable.getPageSize(), response.getTotalPages(), response.getTotalElements());
    }

    @Override
    public PageDTO<ContractResponse> findAllContract(Pageable pageable, String keyword, Set<String> status) {
        keyword = "%" + keyword + "%";
        Page<ContractResponse> response;
        Set<String> finalStatus;
        finalStatus = Objects.requireNonNullElseGet(status, () -> Set.of(StatusContract.PENDING.toString(), StatusContract.APPROVED.toString(), StatusContract.REJECTED.toString(), StatusContract.CANCELED.toString()
                , StatusContract.TIME_OUT.toString()));
        response = contractRepository.findAllByConditionRoleAdmin(keyword, finalStatus, pageable);
        List<ContractResponse> list = response.getContent().stream().peek(temp -> {
            Users user = userRepository.findByUsername(temp.getChangeName());
            if (user != null) {
                temp.setChangeFullName(user.getFullName());
            }
            Users acceptBy = userRepository.findByUsername(temp.getAcceptBy());
            if (acceptBy != null) {
                temp.setAcceptByFull(acceptBy.getFullName());
            }
        }).toList();
        return new PageDTO<>(list, pageable.getPageNumber() + 1, pageable.getPageSize(), response.getTotalPages(), response.getTotalElements());
    }

    @Override
    public Contract accpectOrRejectContract(AcceptOrRejectContractDTO dto) {
        Role role = getCurrentUser().getRole();
        if (Role.ADMIN.equals(role)) {
            Contract contract = contractRepository.findById(dto.getId()).orElseThrow(() -> new ApplicationRuntimeException("Không tìm thấy hợp đồng"));
            if (dto.getStatus().getLevel() < contract.getStatus().getLevel()) {
                throw new ApplicationRuntimeException("Không được phép thay đổi");
            }
            Room room = roomRepository.findById(contract.getRoomId()).orElseThrow(() -> new ApplicationRuntimeException("Không tìm thấy phòng"));
            int studentOfRoom = contractRepository.findAllByRoomIdAndStatus(room.getId(), StatusContract.APPROVED).size();
            Student student = studentRepository.findByUsername(contract.getStudentName()).orElseThrow(() -> new ApplicationRuntimeException("Không tìm thấy sinh viên"));

            if(dto.getStatus() == StatusContract.CANCELED){
                if(studentOfRoom - 1 == 0){
                    room.setStatus(StatusRoom.CHUA_CO_NGUOI);
                } else {
                    room.setStatus(StatusRoom.THIEU_NGUOI);
                }
                roomRepository.save(room);
                student.setRoomId(null);
                studentRepository.save(student);
            } else if(dto.getStatus() == StatusContract.APPROVED){
                if(studentOfRoom + 1 == room.getCapacity()){
                    room.setStatus(StatusRoom.DU_NGUOI);
                } else if(studentOfRoom + 1 < room.getCapacity()){
                    room.setStatus(StatusRoom.THIEU_NGUOI);
                } else {
                    throw new ApplicationRuntimeException("Phòng đã đầy");
                }
                roomRepository.save(room);
                student.setRoomId(room.getId());
                studentRepository.save(student);
            }
            contract.setStatus(dto.getStatus());
            contract.setAcceptBy(getCurrentUser().getUsername());
            contract.setUpdateAt(LocalDate.now());
            contract.setComment(dto.getComment());
            contract.setChangeName(getCurrentUser().getUsername());
            return contractRepository.save(contract);
        } else {
            throw new ApplicationRuntimeException("Bạn không có quyền thực hiện");
        }

    }

    @Override
    public Contract extendContract(ContractDTO contractDTO) {
        Contract contract = contractRepository.findById(contractDTO.getId()).orElseThrow(() -> new ApplicationRuntimeException("Không tìm thấy hợp đồng"));
        contract.setEndAt(contractDTO.getEndAt());
        return contractRepository.save(contract);
    }

}
