package com.da.datn.service.impl;

import com.da.datn.config.exception.ApplicationRuntimeException;
import com.da.datn.dto.EmployeeDTO;
import com.da.datn.dto.PageDTO;
import com.da.datn.dto.RoomDTO;
import com.da.datn.entity.Employee;
import com.da.datn.entity.Room;
import com.da.datn.enums.Role;
import com.da.datn.repository.AdminRepository;
import com.da.datn.repository.EmployeeRepository;
import com.da.datn.service.EmployeeService;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import static com.da.datn.utils.UserUtils.getCurrentUser;

@Slf4j
@Service
@RequiredArgsConstructor
public class EmployeeServiceImpl implements EmployeeService {
    private static final String CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    private static final int ID_LENGTH = 4;

    final EmployeeRepository employeeRepository;
    final PasswordEncoder passwordEncoder;
    final AdminRepository adminRepository;

    @Override
    public PageDTO<EmployeeDTO> getAllEmployee(Pageable pageable, String keyword) {
        Specification<Employee> byRoomNumberLike = (root, query, criteriaBuilder) -> criteriaBuilder.like(criteriaBuilder.lower(root.get("fullName")),"%" + keyword.toLowerCase() + "%");
        Page<Employee> response = employeeRepository.findAll(Specification.where(byRoomNumberLike), pageable);
        List<EmployeeDTO> result = response.getContent().stream().map(employee ->
                EmployeeDTO.builder()
                        .id(employee.getId())
                        .username(employee.getUsername())
                        .email(employee.getEmail())
                        .sex(employee.getGender())
                        .role(employee.getRole())
                        .fullName(employee.getFullName())
                        .imageUrl(employee.getImageUrl())
                        .phoneNumber(employee.getPhoneNumber())
                        .active(employee.getActive())
                        .address(employee.getAddress())
                        .employeeCode(employee.getEmployeeCode())
                        .onBoard(employee.getOnBoard())
                        .offBoard(employee.getOffBoard())
                        .build()
        ).collect(Collectors.toList());
        return new PageDTO<>(result, pageable.getPageNumber() + 1, pageable.getPageSize(), response.getTotalPages(), response.getTotalElements());
    }

    @Override
    public Employee createEmployee(EmployeeDTO employeeDTO) {
        Employee employeeCheck = employeeRepository.findByEmployeeCode(employeeDTO.getEmployeeCode());
        if (employeeCheck != null) {
            throw new ApplicationRuntimeException("Mã nhân viên đã tồn tại");
        }

        Employee employee = Employee.builder()
                .username(employeeDTO.getUsername())
                .password(passwordEncoder.encode(employeeDTO.getPassword()))
                .email(employeeDTO.getEmail())
                .gender(employeeDTO.getSex())
                .role(employeeDTO.getRole())
                .fullName(employeeDTO.getFullName())
                .imageUrl(employeeDTO.getImageUrl())
                .phoneNumber(employeeDTO.getPhoneNumber())
                .active(true)
                .address(employeeDTO.getAddress())
                .employeeCode(generateEmployeeId())
                .onBoard(employeeDTO.getOnBoard())
                .offBoard(employeeDTO.getOffBoard())
                .build();
        try {
            employeeRepository.save(employee);
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new ApplicationRuntimeException("Có lỗi khi tạo nhân viên");
        }
        return employee;
    }

    @Transactional
    @Override
    public void removeEmployee(Long id) {
        try {
            employeeRepository.findById(id).ifPresent(employeeRepository::delete);
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new ApplicationRuntimeException("Có lỗi khi xóa nhân viên");
        }
    }

    @Override
    public Employee updateEmployee(EmployeeDTO employeeDTO) {
        String username = getCurrentUser().getUsername();
        Employee employee = employeeRepository.findById(employeeDTO.getId()).orElseThrow(() -> new ApplicationRuntimeException("Nhân viên không tồn tại"));
        employee.setEmail(employeeDTO.getEmail());
        employee.setGender(employeeDTO.getSex());
        employee.setRole(employeeDTO.getRole());
        employee.setFullName(employeeDTO.getFullName());
        employee.setImageUrl(employeeDTO.getImageUrl());
        employee.setPhoneNumber(employeeDTO.getPhoneNumber());
        employee.setAddress(employeeDTO.getAddress());
        employee.setOnBoard(employeeDTO.getOnBoard());
        employee.setOffBoard(employeeDTO.getOffBoard());
        employee.setChangeName(username);
        return employeeRepository.save(employee);
    }

    @Override
    public List getAcceptBy() {
        if(getCurrentUser().getRole().equals(Role.STUDENT)) {
            return employeeRepository.findAllByActive(true);
        } else {
            return adminRepository.findAllByActive(true);
        }
    }

    private String generateEmployeeId() {
        Random random = new Random();
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < ID_LENGTH; i++) {
            int randomIndex = random.nextInt(CHARACTERS.length());
            sb.append(CHARACTERS.charAt(randomIndex));
        }

        return sb.toString();
    }

}
