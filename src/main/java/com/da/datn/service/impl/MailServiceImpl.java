package com.da.datn.service.impl;

import com.da.datn.config.exception.ApplicationRuntimeException;
import com.da.datn.entity.Student;
import com.da.datn.entity.Users;
import com.da.datn.repository.StudentRepository;
import com.da.datn.repository.UserRepository;
import com.da.datn.service.MailService;
import jakarta.mail.MessagingException;
import jakarta.mail.internet.MimeMessage;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Slf4j
@Service
@RequiredArgsConstructor
public class MailServiceImpl implements MailService {

    final UserRepository userRepository;
    final PasswordEncoder passwordEncoder;
    private final JavaMailSender mailSender;
    final StudentRepository studentRepository;

    @Override
    public void sendMail(String email) {
        MimeMessage message = createMailTemplate(email, "BILL", "");
        mailSender.send(message);
    }

    @Override
    public void sendForgotPasswordMail(String email, String newPassword) {
        MimeMessage message = createMailTemplate(email, "FORGOT_PASSWORD", newPassword);
        mailSender.send(message);
    }

    private MimeMessage createMailTemplate(String email, String type, String newPassword) {
        String fullname = "";
        String imageUrl = "";
        if (type == "BILL") {
            Student student = studentRepository.findByEmail(email);
            fullname = student.getFullName();
            imageUrl = student.getUrlQr();
        } else {
            Users user = userRepository.findByEmail(email).orElseThrow(() -> new ApplicationRuntimeException("User not found"));
            fullname = user.getFullName() == null ? "" : user.getFullName();
        }
        MimeMessage message = mailSender.createMimeMessage();
        LocalDate now = LocalDate.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/yyyy");
        try {
            MimeMessageHelper helper = new MimeMessageHelper(message, true, "utf-8");
            helper.setFrom("ktxhuce.sys@gmail.com", "Mail System");
            helper.setTo(email);
            switch (type) {
                case "BILL":
                    helper.setSubject("Tiền phòng trọ tháng " + now.format(formatter));
                    helper.setText("<html><body>"
                            + "Xin chào " + fullname + ",<br><br>"
                            + "Tiền phòng trọ tháng " + now.format(formatter) + " của bạn là: <br>"
                            + "Vui lòng quét mã QR để thanh toán: <br><br>"
                            + "<img src=\"" + imageUrl + "\" alt=\"QR Code\" /><br><br>"
                            + "Trân trọng,<br>"
                            + "Đội ngũ Huce"
                            + "</body></html>", true);
                    break;
                case "FORGOT_PASSWORD":
                    helper.setSubject("Cấp lại mật khẩu");
                    helper.setText("<html><body>"
                            + "Xin chào " + fullname + ",<br><br>"
                            + "Mật khẩu mới của bạn là: <b>" + newPassword + "</b><br><br>"
                            + "Vui lòng đăng nhập và thay đổi mật khẩu này ngay.<br><br>"
                            + "Trân trọng,<br>"
                            + "Đội ngũ Huce"
                            + "</body></html>", true);
                    break;
            }


        } catch (MessagingException | IOException e) {
            throw new ApplicationRuntimeException("Lỗi gửi mail");
        }
        return message;
    }


}
