package com.da.datn.service.impl;

import com.da.datn.config.exception.ApplicationRuntimeException;
import io.minio.*;
import io.minio.http.Method;
import io.minio.messages.DeleteError;
import io.minio.messages.DeleteObject;
import io.minio.messages.Item;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.coobird.thumbnailator.Thumbnails;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Slf4j
@Service
@RequiredArgsConstructor
public class MinIOService {
    final MinioClient minioClient;
    @Value("${minio.bucket.minio.name}")
    String bucketName;
    @Value("${minio.bucket.qr.name}")
    String bucketQRName;

    public String getUploadQR(String fileName) {
        try {
            return minioClient.getPresignedObjectUrl(
                    GetPresignedObjectUrlArgs.builder()
                            .method(Method.GET)
                            .bucket(bucketQRName)
                            .object(fileName)
                            .expiry(1, TimeUnit.DAYS)
                            .build());
        } catch (Exception e) {
            throw new ApplicationRuntimeException("Có lỗi khi lấy đường dẫn ảnh từ MinIO: ", e);
        }
    }

    public String objectUploadQR(byte[] imageBytes, String fileName) {
        try {
            InputStream inputStream = new ByteArrayInputStream(imageBytes);
            minioClient.putObject(PutObjectArgs.builder()
                    .stream(new BufferedInputStream(inputStream), inputStream.available(), -1)
                    .bucket(bucketQRName)
                    .object(fileName)
                    .contentType("image/jpeg")
                    .build());
            return fileName;
        } catch (Exception e) {
            throw new ApplicationRuntimeException("Có lỗi khi upload ảnh lên: ", e);
        }
    }

    public String objectUpload(MultipartFile file, String bucketName) {
        String uuidImage = UUID.randomUUID().toString();
        String object = uuidImage + "." + FilenameUtils.getExtension(file.getOriginalFilename());
        try {
            var imageobjectNameType = object.substring(object.lastIndexOf(".") + 1);
            BufferedImage originalImage = ImageIO.read(file.getInputStream());
            var width = Math.min(originalImage.getWidth(), 1920);
            byte[] bytes;
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            Thumbnails.of(originalImage)
                    .width(width)
                    .keepAspectRatio(true)
                    .outputFormat(imageobjectNameType)
                    .toOutputStream(outputStream);
            bytes = outputStream.toByteArray();
            InputStream inputStream = new ByteArrayInputStream(bytes);
            minioClient.putObject(PutObjectArgs.builder()
                    .stream(new BufferedInputStream(inputStream), bytes.length, -1)
                    .bucket(bucketName)
                    .object(object)
                    .contentType("image/jpeg")
                    .build());
            return object;
        } catch (Exception e) {
            throw new ApplicationRuntimeException("Có lỗi khi upload ảnh lên: ", e);
        }
    }

    public String getObject(String objectName, String bucketName) {
        try {
            return minioClient.getPresignedObjectUrl(
                    GetPresignedObjectUrlArgs.builder()
                            .method(Method.GET)
                            .bucket(bucketName)
                            .object(objectName)
                            .expiry(1, TimeUnit.DAYS)
                            .build());
        } catch (Exception ignore) {
            return null;
        }
    }


    public List<String> listAllSubfolders(String bucketName, String prefix) {
        List<String> years = listSubfoldersRecursively(bucketName, prefix);
        List<String> result = new ArrayList<>();
        for (String year : years) {
            result.addAll(listSubfoldersRecursively(bucketName, year));
        }
        return result;
    }

    private List<String> listSubfoldersRecursively(String bucketName, String prefix) {
        try {
            Iterable<Result<Item>> results = minioClient.listObjects(
                    ListObjectsArgs.builder()
                            .bucket(bucketName)
                            .prefix(prefix)
                            .recursive(false)  // non-recursive to get top-level directories
                            .build()
            );

            return StreamSupport.stream(results.spliterator(), false)
                    .map(itemResult -> {
                        try {
                            Item item = itemResult.get();
                            String objectName = item.objectName();
                            // Get only the top-level directories
                            int nextSlashIndex = objectName.indexOf('/', prefix.length());
                            if (nextSlashIndex != -1) {
                                return objectName.substring(0, nextSlashIndex + 1);
                            } else {
                                return null; // Not a folder
                            }
                        } catch (Exception e) {
                            throw new ApplicationRuntimeException("Lỗi khi lọc folder minio: ", e);
                        }
                    })
                    .filter(Objects::nonNull)
                    .distinct()
                    .toList();

        } catch (Exception e) {
            throw new ApplicationRuntimeException("Lỗi khi lọc folder minio: ", e);
        }
    }

    public void deleteObjectsByPrefix(String bucketName, String prefix) {
        try {
            // List all objects with the given prefix
            Iterable<Result<Item>> results = minioClient.listObjects(
                    ListObjectsArgs.builder()
                            .bucket(bucketName)
                            .prefix(prefix)
                            .recursive(true)  // Recursively list all objects
                            .build()
            );

            // Collect object names to be deleted
            List<String> objectNames = StreamSupport.stream(results.spliterator(), false)
                    .map(itemResult -> {
                        try {
                            return itemResult.get().objectName();
                        } catch (Exception e) {
                            throw new ApplicationRuntimeException("Lỗi khi xóa ảnh: ", e);
                        }
                    })
                    .filter(Objects::nonNull)
                    .toList();

            // Create a list of DeleteObject for deletion
            List<DeleteObject> objectsToDelete = objectNames.stream()
                    .map(DeleteObject::new)
                    .collect(Collectors.toList());

            // Remove objects
            Iterable<Result<DeleteError>> errorResults = minioClient.removeObjects(
                    RemoveObjectsArgs.builder()
                            .bucket(bucketName)
                            .objects(objectsToDelete)
                            .build()
            );

            // Collect errors, if any
            List<DeleteError> errors = StreamSupport.stream(errorResults.spliterator(), false)
                    .map(deleteErrorResult -> {
                        try {
                            return deleteErrorResult.get();
                        } catch (Exception e) {
                            e.printStackTrace();
                            return null;
                        }
                    })
                    .filter(Objects::nonNull)
                    .toList();

            // Log any errors
            for (DeleteError error : errors) {
                log.error("Có lỗi xảy ra khi xóa ảnh: " + error.objectName() + " due to " + error.message());
                throw new RuntimeException("Có lỗi xảy ra khi xóa ảnh: ");
            }
        } catch (Exception e) {
            throw new RuntimeException("Có lỗi xảy ra khi xóa ảnh: ", e);
        }
    }

    public Optional<ZonedDateTime> getCreationTimeForPrefix(String bucketName, String prefix) {
        try {
            Iterable<Result<Item>> results = minioClient.listObjects(
                    ListObjectsArgs.builder()
                            .bucket(bucketName)
                            .prefix(prefix)
                            .recursive(false)  // non-recursive to get top-level directories
                            .build()
            );

            return StreamSupport.stream(results.spliterator(), false)
                    .map(itemResult -> {
                        try {
                            Item item = itemResult.get();
                            return item.lastModified();
                        } catch (Exception e) {
                            throw new ApplicationRuntimeException("Lỗi khi lấy thời gian tạo folder minio: ", e);
                        }
                    })
                    .filter(Objects::nonNull)
                    .min(ZonedDateTime::compareTo);
        } catch (Exception e) {
            throw new ApplicationRuntimeException("Lỗi khi lấy thời gian tạo folder minio: ", e);
        }
    }
}
