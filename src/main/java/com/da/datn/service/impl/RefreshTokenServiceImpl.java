package com.da.datn.service.impl;

import com.da.datn.config.exception.ApplicationRuntimeException;
import com.da.datn.config.security.JwtTokenUtils;
import com.da.datn.dto.req.RefreshTokenRequest;
import com.da.datn.entity.RefreshToken;
import com.da.datn.entity.Users;
import com.da.datn.repository.RefreshTokenRepository;
import com.da.datn.service.RefreshTokenService;
import com.da.datn.service.UserService;
import io.jsonwebtoken.Jwts;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@RequiredArgsConstructor
public class RefreshTokenServiceImpl implements RefreshTokenService {
    @Value("${application.jwtExpiration}")
    private int jwtExpirationMs;
    @Value("${application.jwtExpirationRefreshToken}")
    private long jwtExpirationRefreshToken;

    private final RefreshTokenRepository refreshTokenRepository;
    private final JwtTokenUtils jwtTokenUtils;
    private final UserService userService;

    @Override
    public Map<String, String> refreshToken(String username, RefreshTokenRequest request) {
        try {
            jwtTokenUtils.validateRefreshJwtToken(request.getRefreshToken());
            RefreshToken refreshToken = refreshTokenRepository.findByRefreshTokenCode(request.getRefreshToken())
                    .orElseThrow(() -> new ApplicationRuntimeException("Lỗi khi lấy lại khóa đăng nhập"));
            if (ObjectUtils.isEmpty(refreshToken)) {
                throw new ApplicationRuntimeException("Phiên đăng nhập hết hạn, vui lòng liên hệ admin", HttpStatus.UNAUTHORIZED);
            }
            Users user = userService.findByUserName(username);
            Map<String, Object> claims = new HashMap<>();
            List<GrantedAuthority> authorities = Collections.
                    singletonList(new SimpleGrantedAuthority(user.getRole().name()));
            String token = Jwts.builder()
                    .setClaims(claims)
                    .setSubject(user.getUsername())
                    .setIssuedAt(new Date())
                    .addClaims(Map.of("authorities", authorities))
                    .setExpiration(new Date((new Date()).getTime() + jwtExpirationMs)).signWith(jwtTokenUtils.getKey()).compact();
            String refreshTokenNew = jwtTokenUtils.generateJwtTokenRefresh();
            refreshTokenRepository.save(RefreshToken.builder()
                    .refreshTokenCode(refreshTokenNew)
                    .userId(user.getId())
                    .expiryDate(new Date((new Date()).getTime() + jwtExpirationRefreshToken))
                    .build());
            return Map.of("token", token, "refreshToken", refreshTokenNew);
        } catch (Exception e) {
            throw new ApplicationRuntimeException("Phiên đăng nhập hết hạn, vui lòng liên hệ admin", HttpStatus.UNAUTHORIZED);
        }
    }
}
