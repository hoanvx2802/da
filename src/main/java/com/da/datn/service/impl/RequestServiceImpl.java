package com.da.datn.service.impl;

import com.da.datn.config.exception.ApplicationRuntimeException;
import com.da.datn.dto.PageDTO;
import com.da.datn.dto.RequestDTO;
import com.da.datn.dto.req.AcceptOrRejectRequestDTO;
import com.da.datn.entity.Request;
import com.da.datn.entity.Room;
import com.da.datn.entity.Users;
import com.da.datn.enums.Role;
import com.da.datn.enums.StatusRequest;
import com.da.datn.repository.RequestRepository;
import com.da.datn.repository.UserRepository;
import com.da.datn.service.RequestService;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;

import static com.da.datn.utils.UserUtils.getCurrentUser;

@Service
@RequiredArgsConstructor
public class RequestServiceImpl implements RequestService {

    final RequestRepository requestRepository;
    final UserRepository userRepository;

    @Override
    public Request create(RequestDTO requestDTO) {

        String username = getCurrentUser().getUsername();
        return requestRepository.save(Request.builder()
                .description(requestDTO.getDescription())
                .status(StatusRequest.PENDING)
                .createName(username)
                .acceptBy(requestDTO.getAcceptBy())
                .userChange(username)
                .createAt(LocalDate.now())
                .updateAt(LocalDate.now())
                .build());
    }

    @Override
    public Request update(RequestDTO requestDTO) {
        String username = getCurrentUser().getUsername();
        Request request = requestRepository.findById(requestDTO.getId()).orElseThrow(() -> new ApplicationRuntimeException("Không tồn tại yêu cầu"));
        if (requestDTO.getStatus().getLevel() > request.getStatus().getLevel()) {
            throw new ApplicationRuntimeException("Không được phép thay đổi");
        } else {
            request.setStatus(requestDTO.getStatus());
            request.setDescription(requestDTO.getDescription());
            request.setAcceptBy(requestDTO.getAcceptBy());
            request.setUpdateAt(LocalDate.now());
            request.setUserChange(username);
            return requestRepository.save(request);
        }
    }

    @Transactional
    @Override
    public void delete(Long id) {
        try {
            requestRepository.findById(id).ifPresent(requestRepository::delete);
        } catch (Exception e) {
            throw new ApplicationRuntimeException("Lỗi khi xóa yêu cầu");
        }
    }

    @Override
    public PageDTO<RequestDTO> findAllRequest(Pageable pageable, String keyword, Set<String> status, LocalDate from, LocalDate to) {
        Users user = getCurrentUser();
        String username = user.getUsername();
        Page<Request> response;

        Specification<Request> byBetween = (root, query, criteriaBuilder) -> {
            if (from == null || to == null) {
                return criteriaBuilder.conjunction();
            }
            return criteriaBuilder.between(root.get("createAt"), from, to);
        };
        Specification<Request> byStatus;
        Set<String> finalStatus;
        if(status == null) {
            finalStatus = Set.of(StatusRequest.APPROVED.toString(), StatusRequest.REJECTED.toString()
                    , StatusRequest.DELETED.toString(), StatusRequest.PENDING.toString());
            byStatus = (root, query, criteriaBuilder)
                    -> root.get("status").in(finalStatus);
        } else {
           finalStatus = status;
            byStatus = (root, query, criteriaBuilder)
                    -> root.get("status").in(finalStatus);
        }

        Specification<Request> byUsername = (root, query, criteriaBuilder)
                -> criteriaBuilder.equal(root.get("createName"), username);
        Specification<Request> byAcceptBy = (root, query, criteriaBuilder)
                -> criteriaBuilder.like(root.get("acceptBy"), "%" + keyword + "%");
        Specification<Request> specCombined = Specification.where(byUsername)
                .and(byUsername)
                .and(byStatus)
                .and(byBetween)
                .and(byAcceptBy);
        response = requestRepository.findAll(Specification.where(specCombined), pageable);


        List<RequestDTO> result = response.getContent().stream().map(temp -> {
            RequestDTO requestDto = new RequestDTO(temp);
            requestDto.setCreateFullName(userRepository.findByUsername(temp.getCreateName()).getFullName());
            requestDto.setAcceptByFull(userRepository.findByUsername(temp.getAcceptBy()).getFullName());
            requestDto.setUserChangeFull(userRepository.findByUsername(temp.getUserChange()).getFullName());
            return requestDto;
        }).toList();
        return new PageDTO<>(result, pageable.getPageNumber() + 1, pageable.getPageSize(), response.getTotalPages(), response.getTotalElements());

    }

    @Override
    public PageDTO<RequestDTO> findAllManagerRequest(Pageable pageable, String keyword, Set<String> status, Boolean isAccept, LocalDate from, LocalDate to) {
        if(isAccept == null){
            isAccept = false;
        }
        Users user = getCurrentUser();
        Role role = user.getRole();
        String username = user.getUsername();
        Page<Request> response;

        Specification<Request> byBetween = (root, query, criteriaBuilder) -> {
            if (from == null || to == null) {
                return criteriaBuilder.conjunction();
            }
            return criteriaBuilder.between(root.get("createAt"), from, to);
        };
        Specification<Request> byStatus;
        Set<String> finalStatus;

        if (role == Role.ADMINISTRATIVE || role == Role.MANAGEMENT_FINANCIAL) {
            if(status == null) {
                if(isAccept){
                    finalStatus = Set.of(StatusRequest.APPROVED.toString(), StatusRequest.REJECTED.toString()
                            , StatusRequest.DELETED.toString(), StatusRequest.CANCELED.toString());
                } else {
                    finalStatus = Set.of(StatusRequest.PENDING.toString());
                }

                byStatus = (root, query, criteriaBuilder)
                        -> root.get("status").in(finalStatus);
            } else {
                finalStatus = status;
                byStatus = (root, query, criteriaBuilder)
                        -> root.get("status").in(finalStatus);
            }

            Specification<Request> byMeAccept = (root, query, criteriaBuilder)
                    -> criteriaBuilder.equal(root.get("acceptBy"), username);

            Specification<Request> specCombined = Specification
                    .where(byMeAccept)
                    .and(byStatus)
                    .and(byBetween);

            response = requestRepository.findAll(Specification.where(specCombined), pageable);
        } else if (role == Role.ADMIN) {
            if(status == null) {
                if(isAccept){
                    finalStatus = Set.of(StatusRequest.APPROVED.toString(), StatusRequest.REJECTED.toString()
                            , StatusRequest.DELETED.toString(), StatusRequest.CANCELED.toString());
                } else {
                    finalStatus = Set.of(StatusRequest.PENDING.toString());
                }

                byStatus = (root, query, criteriaBuilder)
                        -> root.get("status").in(finalStatus);
            } else {
                finalStatus = status;
                byStatus = (root, query, criteriaBuilder)
                        -> root.get("status").in(finalStatus);
            }

            Specification<Request> specCombined = Specification
                    .where(byStatus)
                    .and(byBetween);

            response = requestRepository.findAll(Specification.where(specCombined), pageable);
        } else {
            throw new ApplicationRuntimeException("Thông tin không hợp lệ");
        }

        List<RequestDTO> result = response.getContent().stream().map(temp -> {
            RequestDTO requestDto = new RequestDTO(temp);
            requestDto.setCreateFullName(userRepository.findByUsername(temp.getCreateName()).getFullName());
            requestDto.setAcceptByFull(userRepository.findByUsername(temp.getAcceptBy()).getFullName());
            requestDto.setUserChangeFull(userRepository.findByUsername(temp.getUserChange()).getFullName());
            return requestDto;
        }).toList();
        return new PageDTO<>(result, pageable.getPageNumber() + 1, pageable.getPageSize(), response.getTotalPages(), response.getTotalElements());

    }

    @Override
    public RequestDTO acceptOrRejectRequest(AcceptOrRejectRequestDTO acceptOrRejectRequestDTO) {
        Request request = requestRepository.findById(acceptOrRejectRequestDTO.getId()).orElseThrow(() -> new ApplicationRuntimeException("Không thấy yêu cầu"));
        if (acceptOrRejectRequestDTO.getStatus().getLevel() <= request.getStatus().getLevel()) {
            throw new ApplicationRuntimeException("Không được phép thay đổi");
        }
        request.setStatus(acceptOrRejectRequestDTO.getStatus());
        request.setComment(acceptOrRejectRequestDTO.getComment());
        request.setUpdateAt(LocalDate.now());
        try {
            request = requestRepository.save(request);
            return new RequestDTO(request);
        } catch (Exception e) {
            throw new ApplicationRuntimeException("Lỗi khi phê duyệt yêu cầu");
        }
    }
}
