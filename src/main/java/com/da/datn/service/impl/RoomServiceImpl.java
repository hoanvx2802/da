package com.da.datn.service.impl;

import com.da.datn.config.exception.ApplicationRuntimeException;
import com.da.datn.dto.PageDTO;
import com.da.datn.dto.RoomDTO;
import com.da.datn.entity.Contract;
import com.da.datn.entity.Room;
import com.da.datn.enums.StatusContract;
import com.da.datn.enums.StatusRoom;
import com.da.datn.repository.ContractRepository;
import com.da.datn.repository.RoomRepository;
import com.da.datn.repository.StudentRepository;
import com.da.datn.service.RoomService;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import static com.da.datn.utils.UserUtils.getCurrentUser;


@Service
@RequiredArgsConstructor
public class RoomServiceImpl implements RoomService {
    final RoomRepository roomRepository;
    final ContractRepository contractRepository;
    final StudentRepository studentRepository;

    @Override
    public Room createRoom(RoomDTO roomDTO) {
        Room checkRoom = roomRepository.findByNumberAndBuilding(roomDTO.getNumber(), roomDTO.getBuilding());
        if (checkRoom != null) {
            throw new ApplicationRuntimeException("Room already exists");
        }
        Room room = new Room();
        room.setNumber(roomDTO.getNumber());
        room.setBuilding(roomDTO.getBuilding());
        room.setCapacity(roomDTO.getCapacity());
        room.setAvailable(roomDTO.getAvailable());
        room.setStatus(StatusRoom.CHUA_CO_NGUOI);
        room.setDescription(roomDTO.getDescription());
        room.setPrice(roomDTO.getPrice());
        return roomRepository.save(room);
    }

    @Override
    @Transactional
    public Room updateRoom(RoomDTO roomDTO) {
        try {
            String username = getCurrentUser().getUsername();
            Room room = roomRepository.findById(roomDTO.getId()).orElseThrow(() -> new RuntimeException("Room not found"));
            room.setNumber(roomDTO.getNumber());
            room.setBuilding(roomDTO.getBuilding());
            room.setCapacity(roomDTO.getCapacity());
            room.setAvailable(roomDTO.getAvailable());
            room.setDescription(roomDTO.getDescription());
            room.setPrice(roomDTO.getPrice());
            room.setChangeName(username);
            return roomRepository.save(room);
        } catch (Exception e) {
            throw new ApplicationRuntimeException("Có lỗi khi sửa thông tin phòng");
        }
    }

    @Override
    public PageDTO<RoomDTO> getAllRooms(Pageable pageable, Set<String> status, String building, String roomNumber, Boolean isContract) {
        if (isContract != null && isContract) {
            Specification<Room> byAvaiable = (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("available"), true);
            Specification<Room> combinedSpec = Specification.where(byAvaiable);
            Page<Room> response = roomRepository.findAll(combinedSpec, pageable);

            List<RoomDTO> result = response.getContent().stream().map(room -> {
                        List<Contract> contracts = contractRepository.findByRoomId(room.getId());

                        if (!contracts.isEmpty()) {
                            for(Contract contract: contracts){
                                if(Set.of(StatusContract.APPROVED, StatusContract.PENDING).contains(contract.getStatus())){
                                    return null;
                                }
                            }
                        } else {
                            return RoomDTO.builder()
                                    .id(room.getId())
                                    .number(room.getNumber())
                                    .building(room.getBuilding())
                                    .capacity(room.getCapacity())
                                    .available(room.getAvailable())
                                    .description(room.getDescription())
                                    .status(room.getStatus())
                                    .price(room.getPrice())
                                    .build();
                        }
                        return null;
                    })
                    .filter(Objects::nonNull)
                    .collect(Collectors.toList());
            return new PageDTO<>(result, pageable.getPageNumber() + 1, pageable.getPageSize(), response.getTotalPages(), response.getTotalElements());
        }

        Specification<Room> byRoomNumberLike = (root, query, criteriaBuilder) -> {
            if (roomNumber == null || roomNumber.isEmpty()) {
                return criteriaBuilder.conjunction();
            }
            return criteriaBuilder.like(root.get("number"), "%" + roomNumber + "%");
        };

        Specification<Room> byBuildingLike = (root, query, criteriaBuilder) -> {
            if (building == null || building.isEmpty()) {
                return criteriaBuilder.conjunction();
            }
            return criteriaBuilder.like(root.get("building"), "%" + building + "%");
        };

        Specification<Room> byStatusIn = (root, query, criteriaBuilder) -> {
            if (status == null || status.isEmpty()) {
                return criteriaBuilder.conjunction();
            }
            Set<StatusRoom> statusEnums = status.stream()
                    .map(StatusRoom::valueOf)
                    .collect(Collectors.toSet());
            return root.get("status").in(statusEnums);
        };


        Specification<Room> byAvaiable = (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("available"), true);

        Specification<Room> combinedSpec = Specification.where(byRoomNumberLike)
                .and(byBuildingLike)
                .and(byStatusIn)
                .and(byAvaiable);

        Page<Room> response = roomRepository.findAll(combinedSpec, pageable);

        List<RoomDTO> result = response.getContent().stream().map(room ->
                {
//                    int currentQuantity = studentRepository.findAllByRoomId(room.getId()).size();
                    int currentQuantity = contractRepository.findAllByRoomIdAndStatus(room.getId(), StatusContract.APPROVED).size();
                    return RoomDTO.builder()
                            .id(room.getId())
                            .number(room.getNumber())
                            .building(room.getBuilding())
                            .capacity(room.getCapacity())
                            .available(room.getAvailable())
                            .description(room.getDescription())
                            .status(room.getStatus())
                            .currentQuantity(currentQuantity)
                            .price(room.getPrice())
                            .build();
                }
        ).collect(Collectors.toList());
        return new PageDTO<>(result, pageable.getPageNumber() + 1, pageable.getPageSize(), response.getTotalPages(), response.getTotalElements());
    }

    @Override
    @Transactional
    public void deleteRoom(Long id) {
        try {
            List<Contract> contracts = contractRepository.findByRoomId(id);
            for(Contract contract : contracts){
                if (contract != null && contract.getStatus().equals(StatusContract.APPROVED)) {
                    throw new ApplicationRuntimeException("Phòng đang có hợp đồng, không thể xóa");
                }
                Room room = roomRepository.findById(id).orElseThrow(() -> new RuntimeException("Không tìm thấy phòng"));
                roomRepository.delete(room);
            }
        } catch (Exception e) {
            throw new ApplicationRuntimeException("Có lỗi xảy ra khi xóa phòng");
        }
    }
}
