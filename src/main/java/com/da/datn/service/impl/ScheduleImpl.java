package com.da.datn.service.impl;

import com.da.datn.config.exception.ApplicationRuntimeException;
import com.da.datn.entity.Contract;
import com.da.datn.entity.Employee;
import com.da.datn.enums.StatusContract;
import com.da.datn.repository.ContractRepository;
import com.da.datn.repository.EmployeeRepository;
import io.minio.ListObjectsArgs;
import io.minio.Result;
import io.minio.messages.Item;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.StreamSupport;

@Service
@RequiredArgsConstructor
@Slf4j
public class ScheduleImpl {

    final ContractRepository contractRepository;
    final EmployeeRepository employeeRepository;
    final MinIOService minIOService;
    @Value("${minio.bucket.qr.name}")
    String bucketQRName;
    /*
     * Chạy vào lúc 00:00:00 ngày 1 mỗi tháng
     * Xóa ảnh của nếu đã tạo được 1 năm
     * */
    @Scheduled(cron = "0 0 0 1 * *")
    public void cronDeleteQR() {
        List<String> prefixes = minIOService.listAllSubfolders(bucketQRName, "");
        for (String prefix : prefixes) {
            Optional<ZonedDateTime> creationTimeOpt = minIOService.getCreationTimeForPrefix(bucketQRName, prefix);
            if (creationTimeOpt.isPresent()) {
                ZonedDateTime creationTime = creationTimeOpt.get();
                ZonedDateTime now = ZonedDateTime.now();
                // Xóa ảnh của prefix trong 1 năm
                if (ChronoUnit.DAYS.between(creationTime, now) > 365) {
                    minIOService.deleteObjectsByPrefix(bucketQRName, prefix);
                    log.info("Delete prefix: " + prefix);
                }
            }
        }
    }

    // Mỗi ngày chạy lại status của Hợp đồng đã hết hạn chưa
    @Scheduled(cron = "0 0 1 * * *")
    public void resetStatus(){
        List<Contract> contracts = contractRepository.findAllByStatus(StatusContract.APPROVED);
        for (Contract contract : contracts) {
            if(contract.getEndAt().isEqual(LocalDate.now())){
                contract.setStatus(StatusContract.TIME_OUT);
                contractRepository.save(contract);
            }
        }
        List<Employee> employees = employeeRepository.findAllByActive(true);
        for (Employee employee : employees) {
            if(employee.getOffBoard().isEqual(LocalDate.now())){
                employee.setActive(false);
                employeeRepository.save(employee);
            }
        }
    }
}
