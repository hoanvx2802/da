package com.da.datn.service.impl;

import com.da.datn.config.exception.ApplicationRuntimeException;
import com.da.datn.dto.StatiticsDTO;
import com.da.datn.entity.Room;
import com.da.datn.entity.WaterAndElectric;
import com.da.datn.repository.RoomRepository;
import com.da.datn.repository.SystemConfigRepository;
import com.da.datn.repository.WaterAndElectricRepository;
import com.da.datn.service.StatiticsService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

import static com.da.datn.utils.UserUtils.getCurrentUser;

@Slf4j
@Service
@RequiredArgsConstructor
public class StatiticsServiceImpl implements StatiticsService {

    final RoomRepository roomRepository;
    final WaterAndElectricRepository waterAndElectricRepository;
    final SystemConfigRepository systemConfigRepository;

    @Override
    public String calcBillRoom() {
        List<WaterAndElectric> waterAndElectrics = waterAndElectricRepository.findAllByRoom_Available(false);
        checkRoom(waterAndElectrics);
        Long electricPrice = Long.parseLong(systemConfigRepository.findByKeyword("E_PRICE").orElseThrow(() -> new ApplicationRuntimeException("System config not found")).getValue());
        Long waterPrice = Long.parseLong(systemConfigRepository.findByKeyword("W_PRICE").orElseThrow(() -> new ApplicationRuntimeException("System config not found")).getValue());
        waterAndElectrics.forEach(temp ->
                {
                    calcBillOneRoom(temp.getId(), waterPrice, electricPrice);
                    log.info("Calc bill one room");
                }
        );
        return "Tính hóa đơn thành công";
    }

    private void checkRoom(List<WaterAndElectric> waterAndElectrics) {
        waterAndElectrics.forEach(temp -> {
            Room room = roomRepository.findById(temp.getRoomId()).orElseThrow(() -> new ApplicationRuntimeException("Room not found"));
            if (temp.getStartElectricNumber() == null) {
                throw new ApplicationRuntimeException("Dữ liệu số điện "
                        + " phòng " + room.getNumber()
                        + " tòa " + room.getBuilding()
                        + " thiếu");
            }
            if (temp.getEndElectricNumber() == null) {
                throw new ApplicationRuntimeException("Dữ liệu số điện "
                        + " phòng " + room.getNumber()
                        + " tòa " + room.getBuilding()
                        + " thiếu");
            }
            if (temp.getStartWaterNumber() == null) {
                throw new ApplicationRuntimeException("Dữ liệu số nước "
                        + " phòng " + room.getNumber()
                        + " tòa " + room.getBuilding()
                        + " thiếu");
            }
            if (temp.getEndWaterNumber() == null) {
                throw new ApplicationRuntimeException("Dữ liệu số nước "
                        + " phòng " + room.getNumber()
                        + " tòa " + room.getBuilding()
                        + " thiếu");
            }
        });
    }

    @Override
    public StatiticsDTO getStatiticsForMonth(String month) {
        String year = month.split("/")[0];
        String month1 = month.split("/")[1];
        LocalDate startDate = LocalDate.of(Integer.parseInt(year), Integer.parseInt(month1), 1);
        LocalDate endDate = startDate.plusMonths(1).minusDays(1);
        List<WaterAndElectric> waterAndElectricWithMonth = waterAndElectricRepository.findAllByMonthApplyBetween(startDate, endDate);
        Long totalElectricNumber = waterAndElectricWithMonth.stream()
                .mapToLong(temp -> temp.getEndElectricNumber() - temp.getStartElectricNumber())
                .sum();
        Long totalWaterNumber = waterAndElectricWithMonth.stream()
                .mapToLong(temp -> temp.getEndWaterNumber() - temp.getStartWaterNumber())
                .sum();
        return StatiticsDTO.builder()
                .data(waterAndElectricWithMonth)
                .totalElectricNumber(totalElectricNumber)
                .totalWaterNumber(totalWaterNumber)
                .build();
    }

    @Override
    public String calcBillOneRoom(Long idRoom, Long waterPrice, Long electricPrice) {
        String username = getCurrentUser().getUsername();
        WaterAndElectric waterAndElectric = waterAndElectricRepository.findByRoomId(idRoom).orElseThrow(() -> new ApplicationRuntimeException("Room not found"));
        Room room = roomRepository.findById(waterAndElectric.getRoomId()).orElseThrow(() -> new ApplicationRuntimeException("Room not found"));
        Long numberWater = waterAndElectric.getEndWaterNumber() - waterAndElectric.getStartWaterNumber();
        Long numberElectric = waterAndElectric.getEndElectricNumber() - waterAndElectric.getStartElectricNumber();
        room.setWaterPrice(numberWater * waterPrice);
        room.setElectricPrice(numberElectric * electricPrice);
        room.setChangeName(username);
        roomRepository.save(room);
        return "Tính hóa đơn phòng " + room.getNumber() + " tòa " + room.getBuilding() + " thành công";
    }
}
