package com.da.datn.service.impl;

import com.da.datn.config.exception.ApplicationRuntimeException;
import com.da.datn.dto.PageDTO;
import com.da.datn.dto.StudentDTO;
import com.da.datn.entity.Contract;
import com.da.datn.entity.Room;
import com.da.datn.entity.Student;
import com.da.datn.enums.Role;
import com.da.datn.enums.StatusContract;
import com.da.datn.enums.StatusRoom;
import com.da.datn.repository.ContractRepository;
import com.da.datn.repository.RoomRepository;
import com.da.datn.repository.StudentRepository;
import com.da.datn.service.StudentService;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Set;

import static com.da.datn.utils.UserUtils.getCurrentUser;

@Service
@RequiredArgsConstructor
@Slf4j
public class StudentServiceImpl implements StudentService {

    final StudentRepository studentRepository;

    final PasswordEncoder passwordEncoder;
    final RoomRepository roomRepository;
    final ContractRepository contractRepository;

    @Override
    public PageDTO<StudentDTO> getAllStudents(Pageable pageable, String keyword, Boolean isContract) {
        Specification<Student> byFullNameLike = (root, query, criteriaBuilder) -> criteriaBuilder.like(root.get("fullName"), "%" + keyword + "%");
        Page<Student> response = studentRepository.findAll(Specification.where(byFullNameLike), pageable);
        if (isContract != null && isContract) {
            List<StudentDTO> result = response.getContent().stream()
                    .filter(student -> {
                        Contract contract = contractRepository.findByStudentNameAndStatusIn(student.getUsername(), Set.of(StatusContract.APPROVED, StatusContract.PENDING));
                        return contract == null || !Set.of(StatusContract.APPROVED, StatusContract.PENDING).contains(contract.getStatus());
                    })
                    .map(StudentDTO::new)
                    .toList();
            return new PageDTO<>(result, pageable.getPageNumber() + 1, pageable.getPageSize(), response.getTotalPages(), response.getTotalElements());
        } else {
            List<StudentDTO> result = response.getContent().stream().map(StudentDTO::new).toList();
            return new PageDTO<>(result, pageable.getPageNumber() + 1, pageable.getPageSize(), response.getTotalPages(), response.getTotalElements());
        }
    }

    @Override
    public Student createStudent(StudentDTO studentDTO) {
        Student studentCheck = studentRepository.findByStudentCode(studentDTO.getStudentCode());
        if (studentCheck != null) {
            throw new ApplicationRuntimeException("Mã sinh viên đã tồn tại");
        }

        String status = "Chưa xếp phòng";
        Room room = null;
        if (studentDTO.getRoomId() != null) {
            room = roomRepository.findById(studentDTO.getRoomId()).orElseThrow(() -> new ApplicationRuntimeException("Phong trọ không tồn tại"));
            int studentOfRoom = contractRepository.findAllByRoomIdAndStatus(room.getId(), StatusContract.APPROVED).size();
            if (studentOfRoom + 1 == room.getCapacity()) {
                room.setStatus(StatusRoom.DU_NGUOI);
                roomRepository.save(room);
            }
            status = "Đã xếp phòng";
        }

        Student student = Student.builder()
                .username(studentDTO.getUsername())
                .password(passwordEncoder.encode(studentDTO.getPassword()))
                .email(studentDTO.getEmail())
                .gender(studentDTO.getSex())
                .role(Role.STUDENT)
                .fullName(studentDTO.getFullName())
                .imageUrl(studentDTO.getImageUrl())
                .phoneNumber(studentDTO.getPhoneNumber())
                .active(true)
                .status(status)
                .address(studentDTO.getAddress())
                .studentCode(studentDTO.getStudentCode())
                .className(studentDTO.getClassName())
                .department(studentDTO.getDepartment())
                .emergencyNumber(studentDTO.getEmergencyNumber())
                .roomId(room == null ? null : room.getId())
                .build();
        try{
            studentRepository.save(student);
        } catch (Exception e) {
            throw new ApplicationRuntimeException("Có lỗi khi tạo sinh viên. Vui lòng kiểm tra lại thông tin");
        }
        return student;
    }

    public Student updateStudent(StudentDTO studentDTO) {
        try {
            String username = getCurrentUser().getUsername();
            Student student = studentRepository.findByUsername(studentDTO.getUsername()).orElseThrow(() -> new ApplicationRuntimeException("Không tìm thấy sinh viên"));
            Room oldRoom = null;
            if (studentDTO.getRoomId() != null) {
                oldRoom = roomRepository.findById(studentDTO.getRoomId()).orElseThrow(() -> new ApplicationRuntimeException("Phòng không tồn tại"));
                int studentOfRoom = studentRepository.findAllByRoomId(studentDTO.getRoomId()).size();
                if (oldRoom.getStatus() == StatusRoom.DU_NGUOI
                        && !Objects.equals(student.getRoomId(), studentDTO.getRoomId())) {
                    Room newRoom = roomRepository.findById(studentDTO.getRoomId()).orElseThrow(() -> new ApplicationRuntimeException("Phòng không tồn tại"));
                    if (studentOfRoom - 1 == 0) {
                        oldRoom.setStatus(StatusRoom.CHUA_CO_NGUOI);
                    } else {
                        oldRoom.setStatus(StatusRoom.THIEU_NGUOI);
                    }
                    if(studentOfRoom + 1 == newRoom.getCapacity()) {
                        newRoom.setStatus(StatusRoom.DU_NGUOI);
                    } else {
                        newRoom.setStatus(StatusRoom.THIEU_NGUOI);
                    }
                    roomRepository.save(newRoom);
                } else {
                    if (studentOfRoom + 1 == oldRoom.getCapacity()) {
                        oldRoom.setStatus(StatusRoom.DU_NGUOI);
                    }
                }
                roomRepository.save(oldRoom);
                student.setStatus("Đã xếp phòng");
            }

            student.setEmail(studentDTO.getEmail());
            student.setGender(studentDTO.getSex());
            student.setFullName(studentDTO.getFullName());
            student.setImageUrl(studentDTO.getImageUrl());
            student.setPhoneNumber(studentDTO.getPhoneNumber());
            student.setEmergencyNumber(studentDTO.getEmergencyNumber());
            student.setAddress(studentDTO.getAddress());
            student.setStudentCode(studentDTO.getStudentCode());
            student.setClassName(studentDTO.getClassName());
            student.setDepartment(studentDTO.getDepartment());
            student.setEmergencyNumber(studentDTO.getEmergencyNumber());
            student.setChangeName(username);
            return studentRepository.save(student);
        } catch (Exception e) {
            throw new ApplicationRuntimeException("Có lỗi khi cập nhật sinh viên");
        }
    }


    @Transactional
    @Override
    public void removeStudent(Long id) {
        try {

            studentRepository.findById(id).ifPresent(studentRepository::delete);
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new ApplicationRuntimeException("Có lỗi khi xóa sinh viên");
        }
    }
}
