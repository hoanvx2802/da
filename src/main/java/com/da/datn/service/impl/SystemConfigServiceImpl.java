package com.da.datn.service.impl;

import com.da.datn.config.exception.ApplicationRuntimeException;
import com.da.datn.dto.PageDTO;
import com.da.datn.entity.Room;
import com.da.datn.entity.Student;
import com.da.datn.entity.SystemConfig;
import com.da.datn.repository.RoomRepository;
import com.da.datn.repository.StudentRepository;
import com.da.datn.repository.SystemConfigRepository;
import com.da.datn.service.MailService;
import com.da.datn.service.SystemService;
import io.minio.MinioClient;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Objects;

import static com.da.datn.utils.UserUtils.getCurrentUser;

@Service
@Slf4j
@RequiredArgsConstructor
public class SystemConfigServiceImpl implements SystemService {

    final SystemConfigRepository systemConfigRepository;
    final MinIOService minIOService;
    final RoomRepository roomRepository;
    final MailService mailService;
    final StudentRepository studentRepository;
    private final MinioClient minioClient;

    @Transactional
    @Override
    public SystemConfig addSystemConfig(String keyword, String value) {
        if(keyword == null || value == null) {
            throw new ApplicationRuntimeException("Không được để trống key hoặc value");
        }
        return systemConfigRepository.save(SystemConfig.builder().keyword(keyword).value(value).build());
    }

    @Override
    public SystemConfig updateSystemConfig(String keyword, String value) {
        if(keyword == null || value == null) {
            throw new ApplicationRuntimeException("Không được để trống key hoặc value");
        }
        SystemConfig systemConfig = systemConfigRepository.findByKeyword(keyword).orElseThrow(() -> new ApplicationRuntimeException("System config not found"));
        if (systemConfig != null) {
            systemConfig.setValue(value);
            systemConfig.setChangeName(getCurrentUser().getUsername());
            return systemConfigRepository.save(systemConfig);
        }
        throw new ApplicationRuntimeException("Không có key này. Vui lòng kiểm tra lại");
    }

    //    @Scheduled(cron = "59 59 23 L * ?")
    public void cronSendBill() {
        List<Room> rooms = roomRepository.findAllByAvailable(true);
        for (Room room : rooms) {
            RestTemplate restTemplate = new RestTemplate();
            String bin = "970423";
            String stk = "77728022001";
            String price = "10000";
            String imageGetQRUrl = "https://img.vietqr.io/image/" + bin + "-" + stk + "-qr_only.png?amount=" + price;
            String imageString = "";
            ResponseEntity<byte[]> response = restTemplate.getForEntity(imageGetQRUrl, byte[].class);
            byte[] imageBytes = response.getBody();
            if (imageBytes != null) {
                String fileName = "bill-" + room.getNumber() + LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")) + ".png";

                imageString = minIOService.objectUploadQR(imageBytes, fileName);

            }
            List<Student> students = studentRepository.findAllByRoomId(room.getId());
            String imageUrl = minIOService.getUploadQR(imageString);
            for (Student student : students) {
                student.setUrlQr(imageUrl);
                studentRepository.save(student);
                try {
                    if (!Objects.equals(imageString, "")) {
                        mailService.sendMail(student.getEmail());
                    } else {
                        throw new ApplicationRuntimeException("Error upload QR image");
                    }
                } catch (Exception e) {
                    log.error("Error when send bill to {}", student.getUsername(), e);
                }
            }

        }

    }

    @Override
    public PageDTO<SystemConfig> findAllSystem(Pageable pageable, String keyword) {
        Specification<SystemConfig> byFullNameLike = (root, query, criteriaBuilder) -> criteriaBuilder.like(root.get("keyword"), "%" + keyword + "%");
        Page<SystemConfig> response = systemConfigRepository.findAll(Specification.where(byFullNameLike), pageable);
        return new PageDTO<>(response.getContent(), pageable.getPageNumber() + 1, pageable.getPageSize(), response.getTotalPages(), response.getTotalElements());
    }
}
