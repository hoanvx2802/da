
package com.da.datn.service.impl;

import com.da.datn.config.exception.ApplicationRuntimeException;
import com.da.datn.config.security.UserContext;
import com.da.datn.config.security.UserPrincipal;
import com.da.datn.dto.common.UserDTO;
import com.da.datn.dto.req.ChangePasswordDTO;
import com.da.datn.entity.Users;
import com.da.datn.repository.AdminRepository;
import com.da.datn.repository.UserRepository;
import com.da.datn.service.MailService;
import com.da.datn.service.UserService;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Random;

import static com.da.datn.utils.UserUtils.getCurrentUser;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    final PasswordEncoder passwordEncoder;
    final MailService mailService;
    private static final String CHARACTERS = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789@#$%^&*()_+";
    private static final int ID_LENGTH = 8;

    final UserRepository userRepository;
    @Override
    public Users findByUserName(String username) {
        System.out.println("findByUserName");
        Users users = userRepository.findByUsername(username);
        return users;
    }

    @Override
    public String roleUser(String username) {
        Users users = userRepository.findByUsername(username);
        return users.getRole().toString();
    }

    @Override
    @Transactional
    public void changePassword(ChangePasswordDTO changePasswordDTO) {
        Users users = userRepository.findByUsername(getCurrentUser().getUsername());
        if(!passwordEncoder.matches(changePasswordDTO.getOldPassword(), users.getPassword())){
            throw new ApplicationRuntimeException("Old password is not correct");
        }
        users.setPassword(passwordEncoder.encode(changePasswordDTO.getNewPassword()));
        userRepository.save(users);
    }

    @Override
    public void forgotPassword(String mail) {
        Users user = userRepository.findByEmail(mail).orElseThrow(() -> new ApplicationRuntimeException("User not found"));
        String newPassword = generateRandomPassword();
        String encodedPassword = passwordEncoder.encode(newPassword);
        user.setPassword(encodedPassword);
        userRepository.save(user);
        mailService.sendForgotPasswordMail(mail, newPassword);
    }

    @Override
    public Users updateUser(UserDTO userDTO) {
        try{
            String username = getCurrentUser().getUsername();
            Users users = userRepository.findByUsername(username);
            users.setEmail(userDTO.getEmail());
            users.setFullName(userDTO.getFullName());
            users.setImageUrl(userDTO.getImageUrl());
            users.setPhoneNumber(userDTO.getPhoneNumber());
            users.setCitizenIdentificationCard(userDTO.getCitizenIdentificationCard());
            users.setAddress(userDTO.getAddress());
            users.setChangeName(getCurrentUser().getUsername());
            return userRepository.save(users);
        } catch (Exception e){
            throw new ApplicationRuntimeException("Có lỗi khi cập nhật thông tin");
        }

    }

    private String generateRandomPassword() {
        Random random = new Random();
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < ID_LENGTH; i++) {
            int randomIndex = random.nextInt(CHARACTERS.length());
            sb.append(CHARACTERS.charAt(randomIndex));
        }

        return sb.toString();
    }
}
