package com.da.datn.service.impl;

import com.da.datn.config.exception.ApplicationRuntimeException;
import com.da.datn.dto.InforWaterAndElecDTO;
import com.da.datn.dto.WAEPageDTO;
import com.da.datn.dto.req.InforWAE;
import com.da.datn.entity.Room;
import com.da.datn.entity.WaterAndElectric;
import com.da.datn.enums.StatusContract;
import com.da.datn.enums.StatusWaterAndElectric;
import com.da.datn.repository.*;
import com.da.datn.service.WaterAndElectricService;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

import static com.da.datn.utils.UserUtils.getCurrentUser;

@RequiredArgsConstructor
@Service
@Slf4j
public class WaterAndElectricServiceImpl implements WaterAndElectricService {

    final WaterAndElectricRepository waterAndElectricRepository;
    final EmployeeRepository employeeRepository;
    final SystemConfigRepository systemConfigRepository;
    final RoomRepository roomRepository;
    final UserRepository userRepository;
    private final StudentRepository studentRepository;
    final ContractRepository contractRepository;

    @Override
    public String inputInfor(InforWaterAndElecDTO inforWaterAndElecDTO) {
        WaterAndElectric waterAndElectric = waterAndElectricRepository.findById(inforWaterAndElecDTO.getId()).orElseThrow(() -> new ApplicationRuntimeException("Không tìm thấy dữ liệu cần cập nhật"));
        waterAndElectric.setStartWaterNumber(inforWaterAndElecDTO.getStartWaterNumber());
        waterAndElectric.setEndWaterNumber(inforWaterAndElecDTO.getEndWaterNumber());
        waterAndElectric.setWaterCode(inforWaterAndElecDTO.getWaterCode());
        waterAndElectric.setStartElectricNumber(inforWaterAndElecDTO.getStartElectricNumber());
        waterAndElectric.setEndElectricNumber(inforWaterAndElecDTO.getEndElectricNumber());
        waterAndElectric.setElectricMeterCode(inforWaterAndElecDTO.getElectricMeterCode());
        waterAndElectricRepository.save(waterAndElectric);
        return "Tạo thành công hóa đơn điện nước";
    }

    @Override
    public String updateWaterAndElectric(InforWAE inforWaterAndElecDTO) {
        // Timf role neu laf tai chinh haoc admin thi moi co quyen
        WaterAndElectric waterAndElectric = waterAndElectricRepository.findById(inforWaterAndElecDTO.getId()).orElseThrow(() -> new ApplicationRuntimeException("Không tìm thấy dữ liệu cần cập nhật"));
        if (inforWaterAndElecDTO.getStatus().getLevel() < waterAndElectric.getStatus().getLevel()) {
            throw new ApplicationRuntimeException("Không được phép thay đổi");
        }

        if (waterAndElectric.getEndElectricNumber() != null && inforWaterAndElecDTO.getEndElectricNumber() < waterAndElectric.getEndElectricNumber()) {
            throw new ApplicationRuntimeException("Chỉ số điện mới nhỏ hơn chỉ số điện cũ. Vui lòng kiểm tra lại số điện mới.");
        }

        if (waterAndElectric.getEndWaterNumber() != null && inforWaterAndElecDTO.getEndWaterNumber() < waterAndElectric.getEndWaterNumber()) {
            throw new ApplicationRuntimeException("Chỉ số nước mới nhỏ hơn chỉ số nước cũ. Vui lòng kiểm tra lại số nước mới.");
        }
        waterAndElectric.setStartWaterNumber(inforWaterAndElecDTO.getStartWaterNumber());
        waterAndElectric.setEndWaterNumber(inforWaterAndElecDTO.getEndWaterNumber());
        waterAndElectric.setWaterCode(inforWaterAndElecDTO.getWaterCode());
        waterAndElectric.setStartElectricNumber(inforWaterAndElecDTO.getStartElectricNumber());
        waterAndElectric.setEndElectricNumber(inforWaterAndElecDTO.getEndElectricNumber());
        waterAndElectric.setElectricMeterCode(inforWaterAndElecDTO.getElectricMeterCode());

        long wConfig = Long.parseLong(systemConfigRepository.findByKeyword("W_PRICE").orElseThrow(() -> new ApplicationRuntimeException("System config not found")).getValue());
        long eConfig = Long.parseLong(systemConfigRepository.findByKeyword("E_PRICE").orElseThrow(() -> new ApplicationRuntimeException("System config not found")).getValue());
        Long wPrice = wConfig * (waterAndElectric.getEndWaterNumber() - waterAndElectric.getStartWaterNumber());
        Long ePrice = eConfig * (waterAndElectric.getEndElectricNumber() - waterAndElectric.getStartElectricNumber());
        waterAndElectric.setWaterPrice(wPrice);
        waterAndElectric.setElectricPrice(ePrice);
        waterAndElectricRepository.save(waterAndElectric);
        return "Cập nhật thành công hóa đơn điện nước";
    }

    @Override
    public String deleteWaterAndElectric(Long id) {
        try {
            waterAndElectricRepository.findById(id).ifPresent(waterAndElectricRepository::delete);
        } catch (Exception e) {
            throw new ApplicationRuntimeException("Lỗi khi xóa yêu cầu");
        }
        return "Xóa thành công yêu cầu";
    }

    @Override
    public WAEPageDTO<InforWaterAndElecDTO> getInforWaterAndElec(Pageable pageable, String keyword, String year, String month) {
        LocalDate start = LocalDate.of(Integer.parseInt(year), Integer.parseInt(month), 1);
        LocalDate end = start.plusMonths(1).minusDays(1);
        keyword = "%" + keyword + "%";
        Page<WaterAndElectric> response = waterAndElectricRepository.getInforWaterAndElec(keyword, start, end, pageable);

        List<InforWaterAndElecDTO> result = response.getContent().stream()
                .map(temp -> {
                    Room room = roomRepository.findById(temp.getRoomId())
                            .orElseThrow(() -> new ApplicationRuntimeException("Không tìm thấy phong"));
                    String roomNumber = room.getNumber();
//                    int studentOfRoom = studentRepository.findAllByRoomId(temp.getRoomId()).size();
                    int studentOfRoom = contractRepository.findAllByRoomIdAndStatus(room.getId(), StatusContract.APPROVED).size();
                    return InforWaterAndElecDTO.builder()
                            .id(temp.getId())
                            .employeeName(temp.getEmployeeName())
                            .roomId(temp.getRoomId())
                            .number(roomNumber)
                            .numberOfRoom(studentOfRoom)
                            .monthApply(temp.getMonthApply())
                            .startWaterNumber(temp.getStartWaterNumber())
                            .endWaterNumber(temp.getEndWaterNumber())
                            .waterCode(temp.getWaterCode())
                            .startElectricNumber(temp.getStartElectricNumber())
                            .endElectricNumber(temp.getEndElectricNumber())
                            .electricMeterCode(temp.getElectricMeterCode())
                            .status(temp.getStatus())
                            .waterPrice(temp.getWaterPrice())
                            .electricPrice(temp.getElectricPrice())
                            .roomPrice(room.getPrice())
                            .build();
                })
                .toList();
        long totalElectric = response.getContent().stream()
                .filter(record -> record.getStartElectricNumber() != null && record.getEndElectricNumber() != null)
                .mapToLong(record -> record.getEndElectricNumber() - record.getStartElectricNumber())
                .sum();

        long totalWater = response.getContent().stream()
                        .filter(record -> record.getStartWaterNumber() != null && record.getEndWaterNumber() != null)
                        .mapToLong(record -> record.getEndWaterNumber() - record.getStartWaterNumber())
                        .sum();
        return new WAEPageDTO<>(result,
                pageable.getPageNumber() + 1,
                pageable.getPageSize(),
                response.getTotalPages(),
                response.getTotalElements(), totalWater, totalElectric);
    }

    @Override
    @Transactional
    public String fetch(String year, String month) {
        String username = getCurrentUser().getUsername();
        List<Room> rooms = roomRepository.findAllByAvailable(true);
        LocalDate start = LocalDate.of(Integer.parseInt(Objects.equals(year, null) ? String.valueOf(LocalDate.now().getYear()) : year),
                Integer.parseInt(Objects.equals(month, null) ? String.valueOf(LocalDate.now().getMonthValue()) : month), 1);
        LocalDate end = start.plusMonths(1).minusDays(1);
        LocalDate now = LocalDate.now();
        int currentMonth = now.getMonthValue();
        if (Integer.parseInt(month) > currentMonth) {
            throw new ApplicationRuntimeException("Không thể thực hiện ở thời điểm hiện tại.");
        }
        for (Room room : rooms) {
            WaterAndElectric tempWaterAndElectric = waterAndElectricRepository.findByRoomIdAndMonthApplyBetween(room.getId(), start, end);
            if (tempWaterAndElectric != null) {
                continue;
            }
            tempWaterAndElectric = waterAndElectricRepository.findByRoomIdAndMonthApplyBetween(room.getId(), start.minusMonths(1), start);
            WaterAndElectric waterAndElectric;
            if (tempWaterAndElectric != null) {
                waterAndElectric = WaterAndElectric.builder()
                        .employeeName(tempWaterAndElectric.getEmployeeName())
                        .roomId(tempWaterAndElectric.getRoomId())
                        .createAt(LocalDate.now())
                        .monthApply(start)
                        .startWaterNumber(tempWaterAndElectric.getEndWaterNumber() == null ? 0 : tempWaterAndElectric.getEndWaterNumber())
                        .waterCode(tempWaterAndElectric.getWaterCode() == null ? "W" + room.getNumber() : tempWaterAndElectric.getWaterCode())
                        .startElectricNumber(tempWaterAndElectric.getEndElectricNumber() == null ? 0 : tempWaterAndElectric.getEndElectricNumber())
                        .electricMeterCode(tempWaterAndElectric.getElectricMeterCode() == null ? "E" + room.getNumber() : tempWaterAndElectric.getElectricMeterCode())
                        .status(StatusWaterAndElectric.CREATED)
                        .build();
            } else {
                waterAndElectric = WaterAndElectric.builder()
                        .employeeName(username)
                        .roomId(room.getId())
                        .createAt(LocalDate.now())
                        .monthApply(start)
                        .startWaterNumber(0L)
                        .waterCode("W" + room.getNumber())
                        .startElectricNumber(0L)
                        .electricMeterCode("E" + room.getNumber())
                        .status(StatusWaterAndElectric.CREATED)
                        .build();
            }
            waterAndElectricRepository.save(waterAndElectric);
        }

        return "fetch thành công";
    }
}
