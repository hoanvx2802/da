package com.da.datn.utils;

import com.da.datn.config.exception.ApplicationRuntimeException;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.JpaSort;

public class PageUtils {
    public static Pageable createPageable(int currentPage, int limit, String sort) {
        String[] sortParams = sort.split("-");
        if (sortParams.length == 0) throw new ApplicationRuntimeException("lỗi khi chọn trường để sort");
        String sortBy = String.format("(%s)", sortParams[0]);
        Sort sortPageable = JpaSort.unsafe(sortParams[1].equals(Sort.Direction.ASC.toString()) ? Sort.Direction.ASC : Sort.Direction.DESC, sortBy);
        return PageRequest.of(currentPage - 1, limit, sortPageable);
    }

    public static Pageable getPageable(Integer currentPage, Integer limit, String sort) {
        String[] sortParam = sort.split("-");
        if (sortParam.length == 0) {
            throw new ApplicationRuntimeException("Sai định dạng sắp xếp");
        }
        Sort sortPageable = Sort.by(sortParam[1].equals(Sort.Direction.ASC.toString()) ? Sort.Direction.ASC : Sort.Direction.DESC, sortParam[0]);
        return PageRequest.of(currentPage - 1, limit, sortPageable);
    }
}
