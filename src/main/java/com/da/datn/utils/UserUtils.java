package com.da.datn.utils;

import com.da.datn.config.exception.ApplicationRuntimeException;
import com.da.datn.entity.Users;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

@Slf4j
public class UserUtils {

    public static Users getCurrentUser() {
        try {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            if (authentication != null && authentication.isAuthenticated() && !(authentication.getPrincipal() instanceof String)) {
                Object principal = authentication.getPrincipal();
                return (Users) principal;
            }
            throw new ApplicationRuntimeException("Không xác định được người dùng");
        } catch (Exception e) {
            log.error("Không xác định được người dùng: " + e.getMessage());
            throw new ApplicationRuntimeException("Có lỗi khi xác định được người dùng");
        }
    }
}
